-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 23 Okt 2019 pada 15.23
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `1920_xiirpl_3_07_angginuran`
--
CREATE DATABASE IF NOT EXISTS `1920_xiirpl_3_07_angginuran` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `1920_xiirpl_3_07_angginuran`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

DROP TABLE IF EXISTS `detail_pinjam`;
CREATE TABLE `detail_pinjam` (
  `id_detail` int(11) NOT NULL,
  `id_peminjaman` int(11) DEFAULT NULL,
  `id_inventaris` int(11) DEFAULT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail`, `id_peminjaman`, `id_inventaris`, `jumlah`) VALUES
(1, 4, 5, 2),
(4, 6, 2, 10),
(6, 5, 5, 3),
(7, 7, 1, 5),
(8, 8, 1, 2),
(9, 9, 3, 10),
(10, 10, 2, 3),
(11, 11, 1, 2);

--
-- Trigger `detail_pinjam`
--
DROP TRIGGER IF EXISTS `updateJumlah`;
DELIMITER $$
CREATE TRIGGER `updateJumlah` AFTER INSERT ON `detail_pinjam` FOR EACH ROW begin 
update inventaris set jumlah = jumlah - NEW.jumlah
where id_inventaris = NEW.id_inventaris;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `fpeminjaman`
--

DROP TABLE IF EXISTS `fpeminjaman`;
CREATE TABLE `fpeminjaman` (
  `id_peminjaman` int(10) DEFAULT NULL,
  `nama_peminjam` varchar(30) DEFAULT NULL,
  `nama_barang` varchar(255) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

DROP TABLE IF EXISTS `inventaris`;
CREATE TABLE `inventaris` (
  `id_inventaris` int(10) NOT NULL,
  `id_jenis` int(10) NOT NULL,
  `id_ruang` int(10) NOT NULL,
  `id_operator` int(10) NOT NULL,
  `kode_inventaris` int(10) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `kondisi` enum('baik','rusak') NOT NULL,
  `status` enum('tersedia','habis') NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(10) NOT NULL,
  `tanggal_register` date NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `id_jenis`, `id_ruang`, `id_operator`, `kode_inventaris`, `nama_barang`, `kondisi`, `status`, `keterangan`, `jumlah`, `tanggal_register`, `foto`) VALUES
(1, 1, 1, 1, 101, 'Betadine merahh', 'baik', 'tersedia', 'obat tetes merah1', 25, '2019-07-01', 'betadine.jpg'),
(2, 1, 1, 1, 102, 'kemoceng1', 'baik', 'tersedia', 'alat bersih', 16, '2019-09-09', 'barangPinjam-1568874767-peta.jpg'),
(3, 1, 1, 1, 103, 'kemoceng', 'baik', 'tersedia', 'obat luka', 24, '2019-09-11', 'betadine.jpg'),
(4, 1, 1, 1, 104, 'kemoceng', 'baik', 'tersedia', 'alat pembersih', 10, '2019-10-01', 'barangPinjam-1570347025-home.png'),
(5, 9, 8, 1, 105, 'kabel adapter', 'baik', 'tersedia', 'alat elektronik', 5, '2019-10-14', 'barangPinjam-1570347113-1.8.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

DROP TABLE IF EXISTS `jenis`;
CREATE TABLE `jenis` (
  `id_jenis` int(10) NOT NULL,
  `nama_jenis` varchar(255) NOT NULL,
  `kode_jenis` varchar(10) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'kesehatan', 'J001', 'Jenis Kesehatan1'),
(2, 'teknologi', 'J002', 'jenis teknologi komunikasi'),
(4, 'file pribadi', 'J003', 'file pribadi  ....'),
(8, 'file pribadi1', 'J004', 'file pribadi  ....'),
(9, 'file pribadi1', 'J005', 'file pribadi(khususs)');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jml_inventaris`
--

DROP TABLE IF EXISTS `jml_inventaris`;
CREATE TABLE `jml_inventaris` (
  `sum(jumlah)` decimal(32,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jml_operator`
--

DROP TABLE IF EXISTS `jml_operator`;
CREATE TABLE `jml_operator` (
  `jml_operator` bigint(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jml_peminjam`
--

DROP TABLE IF EXISTS `jml_peminjam`;
CREATE TABLE `jml_peminjam` (
  `jml_peminjam` bigint(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

DROP TABLE IF EXISTS `level`;
CREATE TABLE `level` (
  `id_level` int(10) NOT NULL,
  `nama_level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'operator'),
(3, 'peminjam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `opeator`
--

DROP TABLE IF EXISTS `opeator`;
CREATE TABLE `opeator` (
  `id_operator` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `ava` varchar(255) NOT NULL,
  `id_level` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `opeator`
--

INSERT INTO `opeator` (`id_operator`, `username`, `nama_user`, `password`, `ava`, `id_level`) VALUES
(1, 'gioni', 'm. gioni', 'oni09', 'shawn.jpg', 2),
(3, 'administrator', 'admin', 'admin098', 'cam.jpg', 1),
(15, 'finna', 'finna', '$2y$10$7c4qh2cijB7Kko.TPXK05OPyEV1TZ8cCxjdAOEdRvCT9XxdcQP53i', '', 1),
(16, 'finna1', 'finna1', '$2y$10$XT.qTAPF24GicmsB9KQfQOgT.hPFLrsDSelrG6MbLN2KeUmTjbJmS', '', 1),
(17, 'anggi', 'anggi nuran', '$2y$10$pTAMu2FT8qpI7bsJd/FHs.UitFkZayy70KCeTaHhcxNc6cxSv5ndK', '', 2),
(18, 'petugas', 'petugass', '$2y$10$khI33xCAC62Ss85nk3SFoes22BQylNrZZnExTkHN9x0dtGRQsCrLy', 'ava-1571457141-kuning1.jpg', 2),
(19, 'gioni', 'onii', '$2y$10$zZeojuzJyR0Ovxb9N9c.F.dDfmsLVElIDCKkezznBKH7lMr84We2m', 'ava-1571457504-1.2.png', 2),
(20, 'alya', 'alyaa', '$2y$10$NyblM2FxsrV5hk.YSKvTSucW8qBd9vZpRBvmXr/aqunV1VwSduxgG', 'fotoProfil-1571457141-kuning1.jpg', 1),
(21, 'nadia', 'nadia', '$2y$10$ao3m4OLY6sDV71r/1hQ5ku5B5PPddD6h.anq8eyBEyJkz5fyUP3Mq', 'fotoProfil-1571457956-pacar.jpg', 2),
(22, 'mega', 'mega', '$2y$10$g5.6QrH0.nvaV76EIYNC8uFOnbHPg8z.5Y8zdmsb0gCZu16JK78m.', 'fotoProfil-1571458356-kuning1.jpg', 1),
(23, 'petugas', 'petugass', '$2y$10$T6jWQM3kZcVnD747cNumNOUTM.edhU8IiTgrHO98BaS1W1zV6Hru.', 'fotoProfil-1571478103-akun.png', 2),
(24, 'admin', 'administrator', '$2y$10$3qqBmYseVwlNEY8gtLR9L.6U0jLbivr54c9N.fMaVGReqP/cXiPue', 'fotoProfil-1571478135-akun.png', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjam`
--

DROP TABLE IF EXISTS `peminjam`;
CREATE TABLE `peminjam` (
  `id_peminjam` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `nama_peminjam` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_level` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjam`
--

INSERT INTO `peminjam` (`id_peminjam`, `username`, `nama_peminjam`, `password`, `id_level`) VALUES
(1, 'fisya', 'fisya maurani', 'fish012', 3),
(3, 'tesa', 'tesa p', '$2y$10$vSIShV5osdU5sWX2fjZuC.3', 3),
(4, 'april', 'april', '$2y$10$qJMpz.VNg5amf1qvVdy94um', 3),
(5, 'emakk', 'emak', '$2y$10$5ytpoNfrj0db6Cs.gYFdS.z', 3),
(6, 'finna1', 'finna1', '$2y$10$JbUy53yZXdPmwcrXUbaTYeE', 1),
(7, 'poii', 'poipo', '$2y$10$UdamWGHL2cqD3z7b3sTKK.P', 3),
(8, 'ikan', 'ikan', '$2y$10$ao3m4OLY6sDV71r/1hQ5ku5', 3),
(9, 'ular', 'ular', '$2y$10$8N7Br1itI.QrXNCA2sA3LOMZmNp3dPjosRu7XhUBq6IWfzmrhXejG', 3),
(13, 'peminjam', 'peminjam', '$2y$10$EZAbmng1sUYA6ytCnLdCa.vicG19JZYSBoHTFHJ9lYdA/yDTEVIzy', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

DROP TABLE IF EXISTS `peminjaman`;
CREATE TABLE `peminjaman` (
  `id_peminjaman` int(10) NOT NULL,
  `id_peminjam` int(11) DEFAULT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` enum('Meminjam','Mengembalikan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `id_peminjam`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`) VALUES
(1, 1, '2019-10-01', '2019-10-03', 'Mengembalikan'),
(4, 1, '2019-10-14', '2019-10-16', 'Meminjam'),
(5, NULL, '2019-10-16', '2019-10-28', 'Mengembalikan'),
(6, NULL, '2019-10-16', '2019-10-14', 'Mengembalikan'),
(7, NULL, '2019-10-16', '0000-00-00', 'Mengembalikan'),
(8, NULL, '2019-10-16', '2019-10-24', 'Mengembalikan'),
(9, NULL, '2019-10-17', '2019-10-09', 'Mengembalikan'),
(10, 1, '2019-10-17', '2019-10-19', 'Meminjam'),
(11, 1, '2019-10-18', '2019-10-19', 'Mengembalikan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengembalian`
--

DROP TABLE IF EXISTS `pengembalian`;
CREATE TABLE `pengembalian` (
  `id_pengembalian` int(10) NOT NULL,
  `id_peminjam` int(10) NOT NULL,
  `id_peminjaman` int(10) NOT NULL,
  `id_inventaris` int(11) DEFAULT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengembalian`
--

INSERT INTO `pengembalian` (`id_pengembalian`, `id_peminjam`, `id_peminjaman`, `id_inventaris`, `jumlah`) VALUES
(1, 1, 1, 4, 2),
(3, 1, 1, 2, 3),
(4, 1, 1, 5, 2),
(5, 2, 1, 3, 1),
(6, 1, 1, 1, 3),
(7, 2, 1, 1, 3),
(8, 1, 7, 5, 2);

--
-- Trigger `pengembalian`
--
DROP TRIGGER IF EXISTS `updatePengembalian`;
DELIMITER $$
CREATE TRIGGER `updatePengembalian` AFTER INSERT ON `pengembalian` FOR EACH ROW begin
update inventaris 
set jumlah = jumlah + new.jumlah
where id_inventaris = id_inventaris;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

DROP TABLE IF EXISTS `ruang`;
CREATE TABLE `ruang` (
  `id_ruang` int(10) NOT NULL,
  `nama_ruang` varchar(255) NOT NULL,
  `kode_ruang` varchar(10) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'Lab Rpl1', 'R001', 'Lab Rekayasa Perangkat Lunak'),
(2, 'Uks', 'R002', 'Unit Kesehatan Siswa'),
(8, 'perpustakaan', 'R003', 'Ruangan Perpustakaan'),
(16, 'Lab Rpl 2', 'R004', 'Lab Rekayasa Perangkat Lunak'),
(17, 'Lab MM', 'R005', 'Lab Multimedia');

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `vataris`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `vataris`;
CREATE TABLE `vataris` (
`id_inventaris` int(10)
,`kode_inventaris` int(10)
,`nama_user` varchar(30)
,`nama_ruang` varchar(255)
,`nama_jenis` varchar(255)
,`nama_barang` varchar(255)
,`kondisi` enum('baik','rusak')
,`status` enum('tersedia','habis')
,`keterangan` text
,`jumlah` int(10)
,`tanggal_register` date
,`foto` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `vpe`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `vpe`;
CREATE TABLE `vpe` (
`id_peminjaman` int(10)
,`nama_peminjam` varchar(30)
,`tanggal_pinjam` date
,`tanggal_kembali` date
,`status_peminjaman` enum('Meminjam','Mengembalikan')
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `vpeng`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `vpeng`;
CREATE TABLE `vpeng` (
`id_pengembalian` int(10)
,`nama_peminjam` varchar(30)
,`tanggal_kembali` date
,`status_peminjaman` enum('Meminjam','Mengembalikan')
,`nama_barang` varchar(255)
,`jumlah` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `vtaris`
-- (Lihat di bawah untuk tampilan aktual)
--
DROP VIEW IF EXISTS `vtaris`;
CREATE TABLE `vtaris` (
`id_inventaris` int(10)
,`kode_inventaris` int(10)
,`nama_user` varchar(30)
,`nama_ruang` varchar(255)
,`nama_jenis` varchar(255)
,`nama_barang` varchar(255)
,`kondisi` enum('baik','rusak')
,`status` enum('tersedia','habis')
,`keterangan` text
,`jumlah` int(10)
,`tanggal_register` date
,`foto` varchar(255)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `vataris`
--
DROP TABLE IF EXISTS `vataris`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vataris`  AS  select `i`.`id_inventaris` AS `id_inventaris`,`i`.`kode_inventaris` AS `kode_inventaris`,`o`.`nama_user` AS `nama_user`,`r`.`nama_ruang` AS `nama_ruang`,`j`.`nama_jenis` AS `nama_jenis`,`i`.`nama_barang` AS `nama_barang`,`i`.`kondisi` AS `kondisi`,`i`.`status` AS `status`,`i`.`keterangan` AS `keterangan`,`i`.`jumlah` AS `jumlah`,`i`.`tanggal_register` AS `tanggal_register`,`i`.`foto` AS `foto` from (((`inventaris` `i` join `ruang` `r` on((`i`.`id_ruang` = `r`.`id_ruang`))) join `jenis` `j` on((`i`.`id_jenis` = `j`.`id_jenis`))) join `opeator` `o` on((`i`.`id_operator` = `o`.`id_operator`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `vpe`
--
DROP TABLE IF EXISTS `vpe`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vpe`  AS  select `pe`.`id_peminjaman` AS `id_peminjaman`,`p`.`nama_peminjam` AS `nama_peminjam`,`pe`.`tanggal_pinjam` AS `tanggal_pinjam`,`pe`.`tanggal_kembali` AS `tanggal_kembali`,`pe`.`status_peminjaman` AS `status_peminjaman` from (`peminjaman` `pe` join `peminjam` `p` on((`p`.`id_peminjam` = `pe`.`id_peminjam`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `vpeng`
--
DROP TABLE IF EXISTS `vpeng`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vpeng`  AS  select `p`.`id_pengembalian` AS `id_pengembalian`,`n`.`nama_peminjam` AS `nama_peminjam`,`pe`.`tanggal_kembali` AS `tanggal_kembali`,`pe`.`status_peminjaman` AS `status_peminjaman`,`i`.`nama_barang` AS `nama_barang`,`p`.`jumlah` AS `jumlah` from (((`pengembalian` `p` join `peminjam` `n` on((`p`.`id_peminjam` = `n`.`id_peminjam`))) join `peminjaman` `pe` on((`p`.`id_peminjaman` = `pe`.`id_peminjaman`))) join `inventaris` `i` on((`p`.`id_inventaris` = `i`.`id_inventaris`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `vtaris`
--
DROP TABLE IF EXISTS `vtaris`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vtaris`  AS  select `i`.`id_inventaris` AS `id_inventaris`,`i`.`kode_inventaris` AS `kode_inventaris`,`o`.`nama_user` AS `nama_user`,`r`.`nama_ruang` AS `nama_ruang`,`j`.`nama_jenis` AS `nama_jenis`,`i`.`nama_barang` AS `nama_barang`,`i`.`kondisi` AS `kondisi`,`i`.`status` AS `status`,`i`.`keterangan` AS `keterangan`,`i`.`jumlah` AS `jumlah`,`i`.`tanggal_register` AS `tanggal_register`,`i`.`foto` AS `foto` from (((`inventaris` `i` join `ruang` `r` on((`i`.`id_ruang` = `r`.`id_ruang`))) join `jenis` `j` on((`i`.`id_jenis` = `j`.`id_jenis`))) join `opeator` `o` on((`i`.`id_operator` = `o`.`id_operator`))) where (not((`i`.`jumlah` like 0))) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_operator` (`id_operator`),
  ADD KEY `id_ruang` (`id_ruang`);

--
-- Indeks untuk tabel `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indeks untuk tabel `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indeks untuk tabel `opeator`
--
ALTER TABLE `opeator`
  ADD PRIMARY KEY (`id_operator`),
  ADD KEY `id_level` (`id_level`);

--
-- Indeks untuk tabel `peminjam`
--
ALTER TABLE `peminjam`
  ADD PRIMARY KEY (`id_peminjam`),
  ADD KEY `id_level` (`id_level`);

--
-- Indeks untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_peminjam` (`id_peminjam`);

--
-- Indeks untuk tabel `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `opeator`
--
ALTER TABLE `opeator`
  MODIFY `id_operator` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `peminjam`
--
ALTER TABLE `peminjam`
  MODIFY `id_peminjam` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `inventaris`
--
ALTER TABLE `inventaris`
  ADD CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`),
  ADD CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_operator`) REFERENCES `opeator` (`id_operator`) ON DELETE CASCADE,
  ADD CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`);

--
-- Ketidakleluasaan untuk tabel `opeator`
--
ALTER TABLE `opeator`
  ADD CONSTRAINT `opeator_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`);

--
-- Ketidakleluasaan untuk tabel `peminjam`
--
ALTER TABLE `peminjam`
  ADD CONSTRAINT `peminjam_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`);

--
-- Ketidakleluasaan untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`id_peminjam`) REFERENCES `peminjam` (`id_peminjam`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
