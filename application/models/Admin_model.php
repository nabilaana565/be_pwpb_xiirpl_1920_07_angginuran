<?php

class Admin_model extends CI_Model
{
    private $db_admin;
    private $db_ptgs;
    public function __construct()
    {
        parent::__construct();

        $this->db_admin =  $this->load->database('db_admin', TRUE);
        $this->db_ptgs = $this->load->database('db_ptgs', TRUE);
    }

    public function getAllInven()
    {
        return $this->db_admin->get('vAtaris')->result_array();
    }
    function getAllViewInven()
    {
        return $this->db_admin->get('jml_inventaris')->result_array();
    }
    function getAllViewInven1()
    {
        return $this->db_admin->get('jml_operator')->result_array();
    }
    function getAllViewInven2()
    {
        return $this->db_admin->get('jml_peminjam')->result_array();
    }

    public function getAllPemin()
    {
        return $this->db_admin->get('vPe')->result_array();
    }

    public function getAllUser1()
    {
        return $this->db_admin->get('peminjam')->result_array();
    }
    public function getAllPeng()
    {
        return $this->db_admin->get('vPeng')->result_array();
    }
    public function getAllop()
    {
        $query =   $this->db_admin->get_where('opeator', ['id_level' => 2])->result_array();
        return $query;
    }
    public function getAlljen()
    {
        return $this->db_admin->get('jenis')->result_array();
    }
    public function getAllru()
    {
        return $this->db_admin->get('ruang')->result_array();
    }

    function addInven()
    {
        return $this->db_admin->get('inventaris');
    }

    function input_data($addInven, $table)
    {
        $this->db_admin->insert($table, $addInven);
    }
    function addOp()
    {
        return $this->db_admin->get('opeator');
    }

    function input_dataOp($addOp, $table)
    {
        $this->db_admin->insert($table, $addOp);
    }

    function hapus_data($where, $table)
    {
        $this->db_admin->where($where);
        $this->db_admin->delete($table);
    }
    function edit_data($where, $table)
    {
        return $this->db_admin->get_where($table, $where);
    }
    function update_inventaris($where, $editinven)
    {
        $this->db_admin->where($where);
        return $this->db_admin->update('inventaris', $editinven);
    }
    function get_keywordinven($keywordinven)
    {
        $this->db_admin->select('*');
        $this->db_admin->from('vAtaris');
        $this->db_admin->like('nama_barang', $keywordinven);
        $this->db_admin->or_like('kode_inventaris', $keywordinven);
        return $this->db_admin->get()->result_array();
    }
    function get_keywordpnjmn($keywordpnjmn)
    {
        $this->db_admin->select('*');
        $this->db_admin->from('vPe');
        $this->db_admin->like('nama_peminjam', $keywordpnjmn);
        $this->db_admin->or_like('status_peminjaman', $keywordpnjmn);
        return $this->db_admin->get()->result_array();
    }
    function get_keywordpnjm($keywordpnjm)
    {
        $this->db_admin->select('*');
        $this->db_admin->from('peminjaman');
        $this->db_admin->like('username', $keywordpnjm);
        $this->db_admin->or_like('nama_pengguna', $keywordpnjm);
        return $this->db_admin->get()->result_array();
    }
    function get_keywordpngm($keywordpngm)
    {
        $this->db_admin->select('*');
        $this->db_admin->from('vPeng');
        $this->db_admin->like('nama_peminjam', $keywordpngm);
        $this->db_admin->or_like('nama_barang', $keywordpngm);
        return $this->db_admin->get()->result_array();
    }
    function get_keywordru($keywordru)
    {
        $this->db_admin->select('*');
        $this->db_admin->from('ruang');
        $this->db_admin->like('nama_ruang', $keywordru);
        $this->db_admin->or_like('kode_ruang', $keywordru);
        return $this->db_admin->get()->result_array();
    }
    function get_keywordjen($keywordjen)
    {
        $this->db_admin->select('*');
        $this->db_admin->from('jenis');
        $this->db_admin->like('nama_jenis', $keywordjen);
        $this->db_admin->or_like('kode_jenis', $keywordjen);
        return $this->db_admin->get()->result_array();
    }

    function edit_dataRu($where, $table)
    {
        return $this->db_admin->get_where($table, $where);
    }
    function update_ruang($where, $editruang)
    {
        $this->db_admin->where($where);
        return $this->db_admin->update('ruang', $editruang);
    }
    function detail_vtaris($where, $table)
    {
        return $this->db_admin->get_where($table, $where)->result_array();
    }
    function detail_operator($where, $table)
    {
        return $this->db_admin->get_where($table, $where)->result_array();
    }
    function hapus_operator($where, $table)
    {
        $this->db_admin->where($where);
        $this->db_admin->delete($table);
    }

    function hapus_ruang($where, $table)
    {
        $this->db_admin->where($where);
        $this->db_admin->delete($table);
    }
    function addRu()
    {
        return $this->db_admin->get('ruang');
    }

    function input_dataRu($addRu, $table)
    {
        $this->db_admin->insert($table, $addRu);
    }
    function hapus_jenis($where, $table)
    {
        $this->db_admin->where($where);
        $this->db_admin->delete($table);
    }
    function addjen()
    {
        return $this->db_admin->get('jenis');
    }

    function input_datajen($addjen, $table)
    {
        $this->db_admin->insert($table, $addjen);
    }
    function edit_datajen($where, $table)
    {
        return $this->db_admin->get_where($table, $where);
    }
    function update_jenis($where, $editjenis)
    {
        $this->db_admin->where($where);
        return $this->db_admin->update('jenis', $editjenis);
    }
}
