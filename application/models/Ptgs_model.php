<?php

class Ptgs_model extends CI_Model
{
    private $db_admin;
    private $db_ptgs;
    public function __construct()
    {
        parent::__construct();

        $this->db_admin =  $this->load->database('db_admin', TRUE);
        $this->db_ptgs = $this->load->database('db_ptgs', TRUE);
    }
    // 
    public function getAllInv()
    {
        return $this->db_ptgs->get('inventaris')->result_array();
    }
    public function getAllPm()
    {
        return $this->db_ptgs->get('vPe')->result_array();
    }
    public function getAllPng()
    {
        return $this->db_ptgs->get('vPeng')->result_array();
    }
    function input_dataPemin($addPemin, $table)
    {
        $this->db_ptgs->insert($table, $addPemin);
    }
    function detail_vPe($where, $table)
    {
        return $this->db_ptgs->get_where($table, $where)->result_array();
    }
    function detail_vPeng($where, $table)
    {
        return $this->db_ptgs->get_where($table, $where)->result_array();
    }
    function update_pemin($where, $editPemin)
    {
        $this->db_ptgs->where($where);
        return $this->db_ptgs->update('peminjaman', $editPemin);
    }
    function update_peng($where, $editPeng)
    {
        $this->db_ptgs->where($where);
        return $this->db_ptgs->update('pengembalian', $editPeng);
    }
    function edit_datapemin($where, $table)
    {
        return $this->db_ptgs->get_where($table, $where);
    }
    function delPem($where, $table)
    {
        $this->db_ptgs->where($where);
        $this->db_ptgs->delete($table);
    }
    function input_dataPeng($addPeng, $table)
    {
        $this->db_ptgs->insert($table, $addPeng);
    }
    function delPeng($where, $table)
    {
        $this->db_ptgs->where($where);
        $this->db_ptgs->delete($table);
    }
    // 
    public function getAllPe()
    {
        return $this->db_ptgs->get('peminjam')->result_array();
    }
    function input_dataPem($addPem, $table)
    {
        $this->db_ptgs->insert($table, $addPem);
    }
    function get_keywordinven($keywordinven)
    {
        $this->db_ptgs->select('*');
        $this->db_ptgs->from('inventaris');
        $this->db_ptgs->like('nama_barang', $keywordinven);
        $this->db_ptgs->or_like('kode_inventaris', $keywordinven);
        return $this->db_ptgs->get()->result_array();
    }
    function get_keywordPeng($keywordPeng)
    {
        $this->db_ptgs->select('*');
        $this->db_ptgs->from('vPeng');
        $this->db_ptgs->like('nama_peminjam', $keywordPeng);
        $this->db_ptgs->or_like('nama_barang', $keywordPeng);
        return $this->db_ptgs->get()->result_array();
    }
}
