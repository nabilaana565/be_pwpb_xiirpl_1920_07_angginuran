<?php

class Admin extends CI_Controller
{
    function _construct()
    {
        parent::_construct();


        $this->load->model('Admin_model');
        $this->load->helper('url');
    }
    function index()
    {

        $data['jml_inventaris'] = $this->Admin_model->getAllViewInven();
        $data['jml_operator'] = $this->Admin_model->getAllViewInven1();
        $data['jml_peminjam'] = $this->Admin_model->getAllViewInven2();
        $this->load->view('templates/header', $data);
        $this->load->view('admin/admin', $data);
        $this->load->view('templates/footer', $data);
    }
    function inven()
    {
        $data['vAtaris'] = $this->Admin_model->getAllInven();
        $this->load->view('templates/header', $data);
        $this->load->view('admin/inventaris', $data);
        $this->load->view('templates/footer', $data);
    }
    function print_inventaris()
    {
        $data['inventaris'] = $this->Admin_model->getAllInven('inventaris');
        $this->load->view('admin/print/print_inventaris', $data);
    }
    function search_inven()
    {
        $keywordinven = $this->input->get('keywordinven');

        $data['vAtaris'] = $this->Admin_model->get_keywordinven($keywordinven);
        $this->load->view('templates/header', $data);
        $this->load->view('admin/inventaris', $data);
        $this->load->view('templates/footer', $data);
    }
    function search_pnjmn()
    {
        $keywordpnjmn = $this->input->get('keywordpnjmn');

        $pemin['vPe'] = $this->Admin_model->get_keywordpnjmn($keywordpnjmn);
        $this->load->view('templates/header', $pemin);
        $this->load->view('admin/peminjaman/peminjaman', $pemin);
        $this->load->view('templates/footer', $pemin);
    }
    function search_pngm()
    {
        $keywordpngm = $this->input->get('keywordpngm');

        $peng['vPeng'] = $this->Admin_model->get_keywordpngm($keywordpngm);
        $this->load->view('templates/header', $peng);
        $this->load->view('admin/pengembalian/pengembalian', $peng);
        $this->load->view('templates/footer', $peng);
    }
    function search_pnjm()
    {
        $keywordpnjm = $this->input->get('keywordpnjm');

        $user1['peminjam'] = $this->Admin_model->get_keywordpnjm($keywordpnjm);
        $this->load->view('templates/header', $user1);
        $this->load->view('admin/peminjam/peminjam', $user1);
        $this->load->view('templates/footer', $user1);
    }
    function search_ru()
    {
        $keywordru = $this->input->get('keywordru');

        $ru['ruang'] = $this->Admin_model->get_keywordru($keywordru);
        $this->load->view('templates/header', $ru);
        $this->load->view('admin/ruang/list_ruang');
        $this->load->view('templates/footer', $ru);
    }
    function search_jen()
    {
        $keywordjen = $this->input->get('keywordjen');

        $jen['jenis'] = $this->Admin_model->get_keywordjen($keywordjen);
        $this->load->view('templates/header', $jen);
        $this->load->view('admin/jenis/list_jenis');
        $this->load->view('templates/footer', $jen);
    }
    function peminjaman()
    {
        $pemin['vPe'] = $this->Admin_model->getAllPemin();
        $this->load->view('templates/header', $pemin);
        $this->load->view('admin/peminjaman/peminjaman', $pemin);
        $this->load->view('templates/footer', $pemin);
    }
    function aksidetail_peminjaman()
    {
        $pemin['vPe'] = $this->Admin_model->getAllPemin();
        $this->load->view('templates/header', $pemin);
        $this->load->view('admin/detail_peminjaman', $pemin);
        $this->load->view('templates/footer', $pemin);
    }

    function peminjam()
    {

        $user1['peminjam'] = $this->Admin_model->getAllUser1();
        $this->load->view('templates/header', $user1);
        $this->load->view('admin/peminjam/peminjam', $user1);
        $this->load->view('templates/footer', $user1);
    }
    function aksidetail_peminjam()
    {
        $user1['peminjam'] = $this->Admin_model->getAllUser1();
        $this->load->view('templates/header', $user1);
        $this->load->view('admin/peminjam/detail_peminjam', $user1);
        $this->load->view('templates/footer', $user1);
    }
    function pengembalian()
    {
        $peng['vPeng'] = $this->Admin_model->getAllPeng();
        $this->load->view('templates/header', $peng);
        $this->load->view('admin/pengembalian/pengembalian', $peng);
        $this->load->view('templates/footer', $peng);
    }
    function aksidetail_pengembalian()
    {
        $peng['pengembalian'] = $this->Admin_model->getAllpeng();
        $this->load->view('templates/header', $peng);
        $this->load->view('admin/pengembalian/detail_pengembalian', $peng);
        $this->load->view('templates/footer', $peng);
    }
    function nampilForm()
    {
        $data['ruang'] = $this->db->get('ruang')->result_array();
        $data['opeator'] = $this->db->get('opeator')->result_array();
        $data['jenis'] = $this->db->get('jenis')->result_array();
        $data['inventaris'] = $this->db->get('inventaris')->result_array();
        $data['inventaris'] = $this->db->get('inventaris')->result_array();
        $this->load->view('admin/addinven', $data);
    }

    function aksiadd()
    {
        $config['upload_path']         = 'assets/img/barangPinjam';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 10000;
        // $config['max_width']            = 1024;
        //$config['max_height']           = 768;

        $this->load->library('upload', $config);
        $post           = $_FILES['foto'];
        $file_data         = array(
            "name"     => $post['name'],
            "type"      => $post['type'],
            "tmp_name"  => $post['tmp_name'],
            "error"     => $post['error'],
            "size"      => $post['size']
        );
        $dataUpload     = $file_data;
        $nama_file  = "barangPinjam" . "-" . time() . "-" . str_replace(" ", "_", $post['name']);
        move_uploaded_file($post['tmp_name'], 'assets/img/barangPinjam/' . $nama_file);
        $id_operator = $this->input->post('id_operator');
        $id_jenis = $this->input->post('id_jenis');
        $id_ruang = $this->input->post('id_ruang');
        $kode_inventaris = $this->input->post('kode_inventaris');
        $nama_barang = $this->input->post('nama_barang');
        $kondisi = $this->input->post('kondisi');
        $keterangan = $this->input->post('keterangan');
        $status = $this->input->post('status');
        $jumlah = $this->input->post('jumlah');
        $tanggal_register = $this->input->post('tanggal_register');

        $addinven = array(
            'id_operator' => $id_operator,
            'id_jenis' => $id_jenis,
            'id_ruang' => $id_ruang,
            'kode_inventaris' => $kode_inventaris,
            'nama_barang' => $nama_barang,
            'kondisi' => $kondisi,
            'keterangan' => $keterangan,
            'status' => $status,
            'jumlah' => $jumlah,
            'tanggal_register' => $tanggal_register,
            'foto' => $nama_file
        );
        // var_dump($addinven);
        // die();
        $this->Admin_model->input_data($addinven, 'inventaris');
        redirect('admin/inven');
    }
    function dellInven($id_inventaris)
    {
        $foto = $this->uri->segment(4);
        $where = array('id_inventaris' => $id_inventaris);
        $this->Admin_model->hapus_data($where, 'inventaris');
        unlink('assets/img/barangPinjam/' . $foto);
        redirect('Admin/inven');
    }
    function nampilEdit($id_inventaris)
    {
        $data['ruang'] = $this->db->get('ruang')->result_array();
        $data['opeator'] = $this->db->get('opeator')->result_array();
        $data['jenis'] = $this->db->get('jenis')->result_array();
        $where = array('id_inventaris' => $id_inventaris);
        $data['inventaris'] = $this->Admin_model->edit_data($where, 'inventaris')->result();
        $this->load->view('admin/edit/editInventaris', $data);
    }
    function edit_inven()
    {
        $config['upload_path']         = 'assets/img/barangPinjam';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 10000;
        // $config['max_width']            = 1024;
        //$config['max_height']           = 768;

        $id_inventaris = $this->input->post('id_inventaris');
        $id_operator = $this->input->post('id_operator');
        $id_jenis = $this->input->post('id_jenis');
        $id_ruang = $this->input->post('id_ruang');
        $kode_inventaris = $this->input->post('kode_inventaris');
        $nama_barang = $this->input->post('nama_barang');
        $kondisi = $this->input->post('kondisi');
        $keterangan = $this->input->post('keterangan');
        $status = $this->input->post('status');
        $jumlah = $this->input->post('jumlah');
        $tanggal_register = $this->input->post('tanggal_register');
        $foto2 = $this->input->post('gambar2');

        $post           = $_FILES['foto'];
        $file_data         = array(
            "name"     => $post['name'],
            "type"      => $post['type'],
            "tmp_name"  => $post['tmp_name'],
            "error"     => $post['error'],
            "size"      => $post['size']
        );
        $dataUpload     = $file_data;
        $nama_file  = "barangPinjam" . "-" . time() . "-" . str_replace(" ", "_", $post['name']);
        $upload = move_uploaded_file($post['tmp_name'], 'assets/img/barangPinjam/' . $nama_file);

        $this->load->library('upload', $config);

        if ($upload) {
            $foto =  $nama_file;
            unlink('assets/img/barangPinjam/' . $foto2);
        } else {
            $foto = $this->input->post('gambar2');
        }

        $editinven = array(
            'id_inventaris' => $id_inventaris,
            'id_jenis' => $id_jenis,
            'id_ruang' => $id_ruang,
            'id_operator' => $id_operator,


            'kode_inventaris' => $kode_inventaris,
            'nama_barang' => $nama_barang,
            'kondisi' => $kondisi,
            'status' => $status,
            'keterangan' => $keterangan,

            'jumlah' => $jumlah,
            'tanggal_register' => $tanggal_register,
        );
        $where = array(
            'id_inventaris' => $id_inventaris
        );



        // var_dump($editinven);
        // die();

        $coba = $this->Admin_model->update_inventaris($where, $editinven);
        // var_dump($id_inventaris);
        // die();
        redirect('Admin/inven');
    }


    function aksidetail_inventaris($id_inventaris)
    {
        $where = array(
            'id_inventaris' => $id_inventaris
        );
        $data['vAtaris'] =  $this->Admin_model->detail_vtaris($where, 'vAtaris');
        $this->load->view('templates/header', $data);
        $this->load->view('admin/detail_inventaris', $data);
        $this->load->view('templates/footer', $data);
    }
    function aksidetail_operator($id_operator)
    {
        $where = array(
            'id_operator' => $id_operator
        );
        $op['opeator'] =  $this->Admin_model->detail_operator($where, 'opeator');
        $this->load->view('templates/header', $op);
        $this->load->view('admin/operator/detail_operator', $op);
        $this->load->view('templates/footer', $op);
    }
    function list_operator()
    {
        $op['opeator'] = $this->Admin_model->getAllop();
        $this->load->view('templates/header', $op);
        $this->load->view('admin/operator/list_operator', $op);
        $this->load->view('templates/footer', $op);
    }
    function dellOp($id_operator)
    {

        $where = array('id_operator' => $id_operator);
        $this->Admin_model->hapus_operator($where, 'opeator');
        redirect('Admin/list_operator');
    }
    function nampilFormOp()
    {

        $data['level'] = $this->db->get('level')->result_array();
        $this->load->view('admin/operator/add_operator', $data);
    }

    function aksiaddOp()
    {
        $config['upload_path']         = 'assets/img/fotoProfil/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 10000;
        // $config['max_width']            = 1024;
        //$config['max_height']           = 768;

        $this->load->library('upload', $config);
        $post           = $_FILES['ava'];
        $file_data         = array(
            "name"     => $post['name'],
            "type"      => $post['type'],
            "tmp_name"  => $post['tmp_name'],
            "error"     => $post['error'],
            "size"      => $post['size']
        );
        $dataUpload     = $file_data;
        $nama_file1  = "fotoProfil" . "-" . time() . "-" . str_replace(" ", "_", $post['name']);
        move_uploaded_file($post['tmp_name'], 'assets/img/fotoProfil/' . $nama_file1);
        $username = $this->input->post('username');
        $nama_user = $this->input->post('nama_user');
        $password = $this->input->post('password');
        $id_level = $this->input->post('id_level');


        $addOp = array(
            'username' => $username,
            'nama_user' => $nama_user,
            'password' => password_hash($password, PASSWORD_DEFAULT),
            'id_level' => $id_level,
            'ava' => $nama_file1


        );
        // var_dump($addiOp);
        // die();
        $this->Admin_model->input_dataOp($addOp, 'opeator');
        redirect('admin/list_operator');
    }
    function list_jenis()
    {
        $jen['jenis'] = $this->Admin_model->getAlljen();
        $this->load->view('templates/header', $jen);
        $this->load->view('admin/jenis/list_jenis');
        $this->load->view('templates/footer', $jen);
    }
    function delljen($id_jenis)
    {

        $where = array('id_jenis' => $id_jenis);
        $this->Admin_model->hapus_jenis($where, 'jenis');
        redirect('Admin/list_jenis');
    }
    function nampilFormjen()
    {

        $jenis['jenis'] = $this->db->get('jenis')->result_array();
        $this->load->view('admin/jenis/add_jenis', $jenis);
    }

    function aksiaddjen()
    {
        $nama_jenis = $this->input->post('nama_jenis');
        $kode_jenis = $this->input->post('kode_jenis');
        $keterangan = $this->input->post('keterangan');



        $addjen = array(
            'nama_jenis' => $nama_jenis,
            'kode_jenis' => $kode_jenis,
            'keterangan' => $keterangan
        );
        // var_dump($addiJen);
        // die();
        $this->Admin_model->input_datajen($addjen, 'jenis');
        redirect('admin/list_jenis');
    }
    function nampilEditJen($id_jenis)
    {
        $where = array('id_jenis' => $id_jenis);
        $data['jenis'] = $this->Admin_model->edit_datajen($where, 'jenis')->result();
        $this->load->view('admin/jenis/edit_jenis', $data);
    }
    function edit_jenis()
    {
        $id_jenis = $this->input->post('id_jenis');
        $nama_jenis = $this->input->post('nama_jenis');
        $kode_jenis = $this->input->post('kode_jenis');
        $keterangan = $this->input->post('keterangan');


        $editjenis = array(
            'id_jenis' => $id_jenis,
            'nama_jenis' => $nama_jenis,
            'kode_jenis' => $kode_jenis,
            'keterangan' => $keterangan
        );
        // var_dump($editjenis);
        // die();
        $where = array(
            'id_jenis' => $id_jenis
        );
        $this->Admin_model->update_jenis($where, $editjenis);
        // var_dump($id_inventaris);
        // die();
        redirect('Admin/list_jenis');
    }
    function print_jenis()
    {
        $data['jenis'] = $this->Admin_model->getAlljen('jenis');
        $this->load->view('admin/print/print_jenis', $data);
    }
    // 
    function list_ruang()
    {
        $ru['ruang'] = $this->Admin_model->getAllru();
        $this->load->view('templates/header', $ru);
        $this->load->view('admin/ruang/list_ruang');
        $this->load->view('templates/footer', $ru);
    }
    function dellRu($id_ruang)
    {

        $where = array('id_ruang' => $id_ruang);
        $this->Admin_model->hapus_ruang($where, 'ruang');
        redirect('Admin/list_ruang');
    }
    function nampilFormRu()
    {

        $ruang['ruang'] = $this->db->get('ruang')->result_array();
        $this->load->view('admin/ruang/add_ruang', $ruang);
    }

    function aksiaddRu()
    {
        $nama_ruang = $this->input->post('nama_ruang');
        $kode_ruang = $this->input->post('kode_ruang');
        $keterangan = $this->input->post('keterangan');



        $addRu = array(
            'nama_ruang' => $nama_ruang,
            'kode_ruang' => $kode_ruang,
            'keterangan' => $keterangan
        );
        // var_dump($addiRu);
        // die();
        $this->Admin_model->input_dataRu($addRu, 'ruang');
        redirect('admin/list_ruang');
    }
    function nampilEditRu($id_ruang)
    {
        $where = array('id_ruang' => $id_ruang);
        $data['ruang'] = $this->Admin_model->edit_dataRu($where, 'ruang')->result();
        $this->load->view('admin/ruang/edit_ruang', $data);
    }
    function edit_ruang()
    {
        $id_ruang = $this->input->post('id_ruang');
        $nama_ruang = $this->input->post('nama_ruang');
        $kode_ruang = $this->input->post('kode_ruang');
        $keterangan = $this->input->post('keterangan');


        $editruang = array(
            'id_ruang' => $id_ruang,
            'nama_ruang' => $nama_ruang,
            'kode_ruang' => $kode_ruang,
            'keterangan' => $keterangan
        );
        // var_dump($editruang);
        // die();
        $where = array(
            'id_ruang' => $id_ruang
        );
        $coba = $this->Admin_model->update_ruang($where, $editruang);
        // var_dump($id_inventaris);
        // die();
        redirect('Admin/list_ruang');
    }
}
