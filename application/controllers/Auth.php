<?php

class Auth extends CI_Controller
{
    private $db_admin;
    private $db_ptgs;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('My_model');
        $this->db_admin =  $this->load->database('db_admin', TRUE);
        $this->db_ptgs = $this->load->database('db_ptgs', TRUE);
        $this->load->library('session', 'form_validation');
    }
    public function index()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        if ($this->form_validation->run() == false) {

            $this->load->view('login');
        } else {
            $this->proses_login();
        }
    }


    function proses_login()
    {

        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $where = array(
            'username' => $user,
            'password' => $pass,
        );

        $userPeminjam = $this->db->get_where('peminjam', ['username' => $user])->row_array();
        if ($userPeminjam) {
            // var_dump($userPeminjam);
            // die;
            if (password_verify($pass, $userPeminjam['password'])) {
                $data = [
                    'username' => $userPeminjam['username'],
                    'nama_peminjam' => $userPeminjam['nama_peminjam'],
                    'id_peminjam' => $userPeminjam['id_peminjam'],
                    'id_level' => $userPeminjam['id_level']
                ];
                $this->session->set_userdata($data);
                // var_dump($this->session->userdata('username'));
                // die();
                redirect('Peminjam/index');
            }
        } else {
            $userOperator = $this->db_ptgs->get_where('opeator', ['username' => $user])->row_array();
            if ($userOperator) {

                if (password_verify($pass, $userOperator['password'])  && $userOperator['id_level'] == 2) {
                    $data = [
                        'username' => $userOperator['username'],
                        'id_operator' => $userOperator['id_operator'],
                        'nama_user' => $userOperator['nama_user'],
                        'ava' => $userOperator['ava'],
                        'id_level' => $userOperator['id_level']
                    ];
                    $this->session->set_userdata($data);
                    redirect('petugas/index');
                } else {
                    $userOperator = $this->db_admin->get_where('opeator', ['username' => $user])->row_array();
                    if ($userOperator) {
                        //cek pass
                        if (password_verify($pass, $userOperator['password']) && $userOperator['id_level'] == 1) {
                            $data = [
                                'username' => $userOperator['username'],
                                'id_operator' => $userOperator['id_operator'],
                                'nama_user' => $userOperator['nama_user'],
                                'ava' => $userOperator['ava'],
                                'id_level' => $userOperator['id_level']
                            ];
                            $this->session->set_userdata($data);
                            redirect('admin/index');
                        } else {
                            $data['pesan'] = "Username atau Password tidak sesuai.";
                            $this->load->view('login', $data);
                        }
                    }
                }
            }
        }
    }
    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('level');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Kamu Berhasil Keluar</div>');
        redirect('home');
    }
}
