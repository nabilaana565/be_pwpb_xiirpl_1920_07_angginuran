<?php

class Ainventaris extends CI_Controller
{
    function _construct()
    {
        parent::_construct();


        $this->load->model('Admin_model');
        $this->load->helper('url');
    }
    function nampilForm()
    {
        $data['ruang'] = $this->db->get('ruang')->result_array();
        $data['opeator'] = $this->db->get('opeator')->result_array();
        $data['jenis'] = $this->db->get('jenis')->result_array();
        $data['inventaris'] = $this->db->get('inventaris')->result_array();
        $data['inventaris'] = $this->db->get('inventaris')->result_array();
        $this->load->view('admin/addinven', $data);
    }

    function addInven()
    {
        $id_operator = $this->input->post('id_operator');
        $id_jenis = $this->input->post('id_jenis');
        $id_ruang = $this->input->post('id_ruang');
        $kode_inventaris = $this->input->post('kode_inventaris');
        $nama_barang = $this->input->post('nama_barang');
        $kondisi = $this->input->post('kondisi');
        $status = $this->input->post('status');
        $jumlah = $this->input->post('jumlah');
        $tanggal_register = $this->input->post('tanggal_register');
        $foto = $this->input->post('foto');

        $addinven = array(
            'id_operator' => $id_operator,
            'id_jenis' => $id_jenis,
            'id_ruang' => $id_ruang,
            'kode_inventaris' => $kode_inventaris,
            'nama_barang' => $nama_barang,
            'kondisi' => $kondisi,
            'status' => $status,
            'jumlah' => $jumlah,
            'tanggal_register' => $tanggal_register,
            'foto' => $foto
        );
        $this->Admin_model->input_data($addinven, 'inventaris');
        redirect('admin/inven');
    }
}
