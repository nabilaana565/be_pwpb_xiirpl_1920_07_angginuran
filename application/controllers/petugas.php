<?php

class Petugas extends CI_Controller
{
    function _construct()
    {
        parent::_construct();
        $this->load->model('Ptgs_model');
        $this->load->helper('url');
    }
    function index()
    {
        $this->load->view('templates/header_ptgs');
        $this->load->view('petugas/ptgs');
        $this->load->view('templates/footer_ptgs');
    }

    function inventaris()
    {
        $in['inventaris'] = $this->Ptgs_model->getAllInv('inventaris');
        $this->load->view('templates/header_ptgs');
        $this->load->view('petugas/inventaris', $in);
        $this->load->view('templates/footer_ptgs');

        // $this->load->view('templates/footer', $in);
    }

    function peminjaman()
    {
        $pem['vPe'] = $this->Ptgs_model->getAllPm('vPe');
        $this->load->view('templates/header_ptgs');
        $this->load->view('petugas/peminjaman', $pem);
        $this->load->view('templates/footer_ptgs');

        // $this->load->view('templates/footer', $pem);
    }

    function pengembalian()
    {

        $png['vPeng'] = $this->Ptgs_model->getAllPng('vPeng');
        $this->load->view('templates/header_ptgs');
        $this->load->view('petugas/pengembalian', $png);
        $this->load->view('templates/footer_ptgs');
        // $this->load->view('templates/footer', $png);
    }
    function nampilFormpemin($id_peminjaman)
    {

        $where = array('id_peminjaman' => $id_peminjaman);
        $data['peminjam'] = $this->db->get('peminjam')->result_array();
        $data['peminjaman'] = $this->Ptgs_model->edit_dataPemin($where, 'peminjaman')->result();
        $this->load->view('petugas/edit/e_peminjaman', $data);
    }
    function aksiEditPemin()
    {

        $id_peminjaman = $this->input->post('id_peminjaman');
        $id_peminjam = $this->input->post('id_peminjam');
        $tanggal_pinjam = $this->input->post('tanggal_pinjam');
        $tanggal_kembali = $this->input->post('tanggal_kembali');
        $status_peminjaman = $this->input->post('status_peminjaman');


        $editPemin = array(
            'id_peminjaman' => $id_peminjaman,
            'id_peminjam' => $id_peminjam,
            'tanggal_pinjam' => $tanggal_pinjam,
            'tanggal_kembali' => $tanggal_kembali,
            'status_peminjaman' => $status_peminjaman
        );
        $where = array(
            'id_peminjaman' => $id_peminjaman
        );
        $this->Ptgs_model->update_pemin($where, $editPemin);
        redirect('petugas/peminjaman');
    }
    function det_peminjaman($id_peminjaman)
    {
        $where = array(
            'id_peminjaman' => $id_peminjaman
        );
        $data['vPe'] =  $this->Ptgs_model->detail_vPe($where, 'vPe');
        $this->load->view('petugas/detail/d_peminjaman', $data);
    }
    function dellPem($id_peminjaman)
    {

        $where = array('id_peminjaman' => $id_peminjaman);
        $this->Ptgs_model->delPem($where, 'peminjaman');
        redirect('petugas/peminjaman');
    }
    function det_pengembalian($id_pengembalian)
    {
        $where = array(
            'id_pengembalian' => $id_pengembalian
        );
        $data['vPeng'] =  $this->Ptgs_model->detail_vPeng($where, 'vPeng');
        $this->load->view('petugas/detail/d_pengembalian', $data);
    }
    function dellPeng($id_pengembalian)
    {

        $where = array('id_pengembalian' => $id_pengembalian);
        $this->Ptgs_model->delPeng($where, 'pengembalian');
        redirect('petugas/pengembalian');
    }
    function nampilFormpeng($id_pengembalian)
    {
        $data['inventaris'] = $this->db->get('inventaris')->result_array();
        $data['peminjam'] = $this->db->get('peminjam')->result_array();
        $data['peminjaman'] = $this->db->get('peminjaman')->result_array();
        $where = array('id_pengembalian' => $id_pengembalian);
        $data['pengembalian'] = $this->Ptgs_model->edit_dataPemin($where, 'pengembalian')->result();
        $this->load->view('petugas/edit/e_pengembalian', $data);
    }
    function nampilFormAddpeng()
    {
        $data['inventaris'] = $this->db->get('inventaris')->result_array();
        $data['peminjam'] = $this->db->get('peminjam')->result_array();
        $data['peminjaman'] = $this->db->get('peminjaman')->result_array();
        $data['pengembalian'] = $this->db->get('pengembalian')->result_array();
        $this->load->view('petugas/addpengembalian', $data);
    }
    function aksiEditPeng()
    {

        $id_pengembalian = $this->input->post('id_pengembalian');
        $id_peminjam = $this->input->post('id_peminjam');
        $id_peminjaman = $this->input->post('id_peminjaman');
        $id_inventaris = $this->input->post('id_inventaris');
        $jumlah = $this->input->post('jumlah');


        $editPeng = array(
            'id_pengembalian' => $id_pengembalian,
            'id_peminjam' => $id_peminjam,
            'id_peminjaman' => $id_peminjaman,
            'id_inventaris' => $id_inventaris,
            'jumlah' => $jumlah
        );
        $where = array(
            'id_pengembalian' => $id_pengembalian
        );
        $this->Ptgs_model->update_peng($where, $editPeng);
        redirect('petugas/pengembalian');
    }

    function aksiAddPeng()
    {
        $id_peminjam = $this->input->post('id_peminjam');
        $id_peminjaman = $this->input->post('id_peminjaman');
        $id_inventaris = $this->input->post('id_inventaris');
        $jumlah = $this->input->post('jumlah');


        $addPeng = array(
            'id_peminjam' => $id_peminjam,
            'id_peminjaman' => $id_peminjaman,
            'id_inventaris' => $id_inventaris,
            'jumlah' => $jumlah
        );
        $this->Ptgs_model->input_dataPeng($addPeng, 'pengembalian');
        redirect('petugas/pengembalian');
    }
    function print_peminjaman()
    {
        $data['vPe'] = $this->Ptgs_model->getAllPm('vPe');
        $this->load->view('petugas/print/peminjaman', $data);
    }
    function print_pengembalian()
    {
        $data['vPeng'] = $this->Ptgs_model->getAllPng('vPeng');
        $this->load->view('petugas/print/pengembalian', $data);
    }

    function peminjam()
    {
        $pe['peminjam'] = $this->Ptgs_model->getAllPe();
        $this->load->view('templates/header_ptgs', $pe);
        $this->load->view('petugas/peminjam', $pe);
        $this->load->view('templates/footer_ptgs', $pe);
    }
    // function dellOp($id_operator)
    // {

    //     $where = array('id_operator' => $id_operator);
    //     $this->Admin_model->hapus_operator($where, 'peminjam');
    //     redirect('Admin/list_operator');
    // }
    function nampilFormPem()
    {

        $data['level'] = $this->db->get('level')->result_array();
        $this->load->view('petugas/add_peminjam', $data);
    }
    function aksiaddPem()
    {
        $username = $this->input->post('username');
        $nama_peminjam = $this->input->post('nama_peminjam');
        $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
        $id_level = $this->input->post('id_level');


        $addPem = array(
            'username' => $username,
            'nama_peminjam' => $nama_peminjam,
            'password' => $password,
            'id_level' => $id_level

        );
        // var_dump($addPem);
        // die();
        $this->Ptgs_model->input_dataPem($addPem, 'peminjam');
        redirect('petugas/peminjam');
    }
    function search_inven()
    {
        $keywordinven = $this->input->get('keywordinven');

        $data['inventaris'] = $this->Ptgs_model->get_keywordinven($keywordinven);
        $this->load->view('templates/header_ptgs', $data);
        $this->load->view('petugas/inventaris', $data);
        $this->load->view('templates/footer_ptgs', $data);
    }
    function search_peng()
    {
        $keywordPeng = $this->input->get('keywordPeng');

        $png['vPeng'] = $this->Ptgs_model->get_keywordPeng($keywordPeng);
        $this->load->view('templates/header_ptgs', $png);
        $this->load->view('petugas/pengembalian', $png);
        $this->load->view('templates/footer_ptgs', $png);
    }
}
