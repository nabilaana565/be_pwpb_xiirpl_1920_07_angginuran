<?php

class Peminjam extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Inventaris_model');
        $this->load->helper('url');
    }

    public function index()
    {
        // $userPeminjam = $this->db->get_where('peminjam', ['username' => $this->session->userdata('username')])->row_array();
        $data['vtaris'] = $this->db->get('vtaris')->result_array();
        $data['inventaris'] = $this->Inventaris_model->getAllInventaris();
        $this->load->view('peminjam/peminjam', $data);
    }

    function aksidetail_pnjmn($id_inventaris)
    {
        //  = $this->db->get_where('peminjam', ['username' => $this->session->userdata('username')])->row_array();
        $where = array(
            'id_inventaris' => $id_inventaris
        );
        $data['vtaris'] =  $this->Inventaris_model->detail_pnjmn($where, 'vtaris');
        $this->load->view('peminjam/detail_pnjmn', $data);
    }

    function nampilFormPem($id_inventaris)
    {
        //  = $this->db->get_where('peminjam', ['username' => $this->session->userdata('username')])->row_array();
        $where = array(
            'id_inventaris' => $id_inventaris
        );
        $data['inventaris'] =  $this->Inventaris_model->detail_pnjmn($where, 'inventaris');
        $this->load->view('peminjam/formPinjam', $data);
    }

    function tambah_pjm()
    {
        //  = $this->db->get_where('peminjam', ['username' => $this->session->userdata('username')])->row_array();
        // $tgl_pinjam = $this->input->post('tgl_pinjam');
        $tanggal_kembali = $this->input->post('tanggal_kembali');
        // $status_peminjaman = $this->input->post('status_peminjaman');
        // $id_peminjam = $this->input->post('id_peminjam');

        $data = array(
            'tanggal_pinjam' => date('Y-m-d'),
            'tanggal_kembali' => $tanggal_kembali,
            'status_peminjaman' => 'meminjam',
            'id_peminjam' => $this->session->userdata('id_peminjam'),
        );
        $id_peminjaman = $this->Pmnjmn_model->input($data, 'peminjaman');

        $id_inventaris = $this->input->post('id_inventaris');
        $jumlah = $this->input->post('jumlah');
        //$id_peminjaman =  $this->db->insert_id();

        $index = 0;

        $data = array(
            'id_peminjaman' => $id_peminjaman,
            'id_inventaris' => $id_inventaris,
            'jumlah' => $jumlah
        );
        $this->db->insert('detail_pinjam', $data);
        redirect('peminjam/index');
    }
    function search_inventaris()
    {
        //  = $this->db->get_where('peminjam', ['username' => $this->session->userdata('username')])->row_array();
        $keywordinventaris = $this->input->get('keywordinventaris');

        $data['vtaris'] = $this->Pmnjmn_model->get_keywordinventaris($keywordinventaris);
        $this->load->view('peminjam/peminjam', $data);
    }
}
