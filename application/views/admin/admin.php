<!-- Page Content -->
<div id="page-content-wrapper">

  <nav class="navbar navbar-expand-lg border-bottom" style="background-color: #33AAC5; ">
    <button class="btn" id="menu-toggle"><i class="fas fa-bars" style="color: #fff"></i></button>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">




      <ul class="navbar-nav ml-auto mt-2 mt-lg-0">

        <li class="nav-item" style="margin-left:5px">
          <div class="txt" style="color:white;"> <?= $this->session->userdata('username'); ?></div>
        </li>
        <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
        </li> -->
      </ul>
    </div>
  </nav>
  <br>
  <div class="container-fluid">
    <div class="jumbotron" style="background-color: #9abce1;">
      <div class="container text-center">
        <h1>Dashboard Admin</h1>
        <p>Selamat Datang Admin.</p>
      </div>
    </div>
    <br>

    <div class="row left">


      <div class="col-md-4">
        <?php foreach ($jml_inventaris as $jml1) : ?>
          <div class="kotak1">
            <br>
            <center>
              <h6>Total Inventaris</h6>
              <h4><?= $jml1['sum(jumlah)'] ?></h4>
            </center>
          </div>
        <?php endforeach ?>
      </div>


      <div class="col-md-4">
        <?php foreach ($jml_peminjam as $pem) : ?>
          <div class="kotak2">
            <br>
            <center>
              <h6>Total Peminjam</h6>
              <h4><?= $pem['jml_peminjam'] ?></h4>
            </center>
          </div>
        <?php endforeach ?>

      </div>
      <div class="col-md-4">

        <?php foreach ($jml_operator as $peng) : ?>
          <div class="kotak3">
            <br>
            <center>
              <h6>Total Operator</h6>
              <h4><?= $peng['jml_operator'] ?></h4>
            </center>
          </div>
        <?php endforeach ?>


      </div>

    </div>
    <br>
    <center>
      <div class="row">
        <div class="col-sm-4">
          <a href="<?= base_url(); ?>Admin/inven"><i class="fas fa-boxes" style="font-size:48px;color:#4DB6AC"></i></a>
          <h4>Inventaris</h4>
          <p>List Inventaris</p>
        </div>
        <div class="col-sm-4">
          <a href="<?= base_url(); ?>Admin/peminjam"><i class="fa fa-users" style="font-size:48px;color:#4DB6AC"></i></a>
          <br>
          <h4>Peminjam</h4>
          <p>List Peminjam</p>
        </div>
        <div class="col-sm-4">
          <a href="<?= base_url(); ?>Admin/list_jenis"><i class=" fa fa-sticky-note" style="font-size:48px;color:#4DB6AC"></i></a>
          <h4>Jenis</h4>
          <p>List Jenis Inventaris</p>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-sm-4">
          <a href="<?= base_url(); ?>Admin/peminjaman"><i class="fas fa-memory" style="font-size:48px;color:#4DB6AC"></i></a>
          <h4>Peminjaman</h4>
          <p>Data Peminjaman</p>
        </div>
        <div class="col-sm-4">
          <a href="<?= base_url(); ?>Admin/list_ruang"><i class="far fa-building" style="font-size:48px;color:#4DB6AC"></i></a>
          <br>
          <h4>Ruang</h4>
          <p>Data Ruang</p>
        </div>
        <div class="col-sm-4">
          <a href="<?= base_url(); ?>Admin/list_operator"><i class="fas fa-id-card-alt" style="font-size:48px;color:#4DB6AC"></i></a>
          <h4>Operator</h4>
          <p>ListOperator</p>
        </div>
      </div>
    </center>
  </div> <br> <br>
</div><!-- /.row -->




</div>
</div>


</div>







<!-- /#page-content-wrapper -->

</div>

<!-- div4 -->





</div>