<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
     <link rel="stylesheet" href="../../assets/css/style.css">

    <title>Edit Peminjaman</title>
  </head>

  <body id="rg">

     <a href="peminjaman.html">
  <img src="../../assets/img/icon/panah.png"  title="kembali" class="keluar" style="margin: 20px;">
     </a>

      <div class="fp">
        <div class="t1" style="background-color:#BADDE8; height: 30%; padding: 8px 0px;">
        <h2><center>Edit Data Peminjaman</center></h2> 

        </div>
<br>
        <form style="margin-top: 10px;">
  <div class="form-group">
    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">id_pengembalian</label>

    <input type="id_pengembalian" class="form-control" id="exampleInputid_pengembalian1" aria-describedby="id_pengembalianHelp" placeholder="id_pengembalian" style="width:95%;">
  </div>

  <div class="form-group">
    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">id_peminjam</label>

    <input type="id_pinjam" class="form-control" id="exampleInputid_pinjam1" aria-describedby="id_pinjamHelp" placeholder="id_pinjam" style="width:95%;">
  </div>


  <div class="form-group">
    <label for="staticEmail" class="col-sm-2 col-form-label" style="width: 100%;">Nama Barang </label>

    <input type="namaBarang" class="form-control" id="exampleInputnamaBarang1" aria-describedby="namaBarangHelp" placeholder="Nama Barang" style="width:95%;">
  </div>


  <div class="form-group">
    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="width: 100%;">Tanggal Pinjam</label>

    <input type="date" class="form-control" id="exampleInputtanggalPinjam1" aria-describedby="tanggalPinjamHelp" placeholder="tanggalPinjam" style="width:95%;">
  </div>
  
  <button type="submit" class="btn btn-primary" style="margin:0px 3%; margin-bottom: 20px;">Submit</button>
</form>
      </div>

        <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/npm.js"></script>

    <script src="bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>