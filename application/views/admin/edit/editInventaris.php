<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">

    <title>Edit Inventaris</title>
    <style>
        .form-control {
            width: 80%;
        }

        label {
            margin-left: 20px;
        }

        .input-group {
            margin-left: 10px;
            margin-right: 10px;


        }
    </style>
</head>

<body id="rg">

    <!-- <a href="peminjaman.html">
        <img src="../assets/img/icon/panah.png" title="kembali" class="keluar" style="margin: 20px;">
    </a> -->

    <div class="fp">
        <div class="t1" style="background-color:#BADDE8; height: 30%; padding: 8px 0px;">
            <h2>
                <center style="color:white;">EDIT INVENTARIS</center>
            </h2>

        </div>
        <br>
        <?php foreach ($inventaris as $ven) { ?>
            <form style="margin-top: 10px;" action="<?= base_url('Admin/edit_inven'); ?> " method="post" enctype="multipart/form-data">

                <div class="form-group row">
                    <div class="col-xs-4">
                        <label for="sel1">ID Operator</label>
                        <select class="form-control" id="sel1" name="id_operator" value="<?= $ven->id_operator ?>">
                            <?php foreach ($opeator as $o) : ?>
                                <option value="<?= $o['id_operator'] ?>"><?= $o['nama_user']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <label for="sel1">ID Ruang</label>
                        <select class="form-control" id="sel1" name="id_ruang" value="<?= $ven->id_ruang ?>">
                            <?php foreach ($ruang as $r) : ?>
                                <option value="<?= $r['id_ruang'] ?>"><?= $r['nama_ruang']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <label for="sel1">ID Jenis</label>
                        <select class="form-control" id="sel1" name="id_jenis" value="<?= $ven->id_jenis ?>">
                            <?php foreach ($jenis as $j) : ?>
                                <option value="<?= $j['id_jenis'] ?>"><?= $j['nama_jenis']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-barcode"></i></span>
                    <input type="hidden" class="form-control" placeholder="Kode Inventaris" aria-describedby="sizing-addon2" name="id_inventaris" value="<?= $ven->id_inventaris ?>">
                    <input type="text" class="form-control" placeholder="Kode Inventaris" aria-describedby="sizing-addon2" name="kode_inventaris" value="<?= $ven->kode_inventaris ?>">
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-folder-close"></i></span>
                    <input type="text" class="form-control" placeholder="Nama Barang" aria-describedby="sizing-addon2" name="nama_barang" value="<?= $ven->nama_barang ?>">
                </div>

                <br>

                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-align-justify"></i></span>
                    <input type="text" class="form-control" placeholder="Keterangan" aria-describedby="sizing-addon2" name="keterangan" value="<?= $ven->keterangan ?>">
                </div>

                <br>
                <div class="form-group row">
                    <div class="col-xs-4">
                        <label for="sel1">kondisi</label>
                        <select class="form-control" id="sel1" name="kondisi" value="<?= $ven->kondisi ?>">

                            <option value="baik"> Baik</option>
                            <option value="rusak"> rusak</option>

                        </select>
                    </div>
                    <div class="col-xs-4">
                        <label for="sel1">Status</label>
                        <select class="form-control" id="sel1" name="status" value="<?= $ven->status ?>">
                            <option value="tersedia"> tersedia</option>
                            <option value="habis"> Habis</option>
                        </select>
                    </div>

                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-list-alt"></i></span>
                    <input type="text" class="form-control" placeholder="Jumlah" aria-describedby="sizing-addon2" name="jumlah" value="<?= $ven->jumlah ?>">
                </div>
                <br>

                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-calendar"></i></span>
                    <input type="date" class="form-control" aria-describedby="sizing-addon2" name="tanggal_register" value="<?= $ven->tanggal_register ?>">
                </div> <br>

                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="file" class="form-control" placeholder="Image" aria-describedby="sizing-addon2" name="gambar2" value="<?= $ven->foto ?>">
                </div> <br>
                <input type="submit" class="btn btn-primary" style="margin:0px 3%; margin-bottom: 20px;" value="kirim" onclick="return confirm ('Simpan Perubahan'); ">
                <a href="<?= base_url(); ?> Admin/inven"><input type="submit" class="btn btn-danger" style="margin:0px 0%; margin-bottom: 20px;" value="cancel" onclick="return confirm ('Batalkan Perubahan'); "></a>

            </form>
        <?php  } ?>
    </div> <br>
    <br>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/npm.js"></script>

    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>