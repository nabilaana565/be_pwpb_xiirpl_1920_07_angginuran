<!-- Page Content -->
<div id="page-content-wrapper">

  <nav class="navbar navbar-expand-lg border-bottom" style="background-color: #33AAC5; ">
    <button class="btn" id="menu-toggle"><i class="fas fa-bars" style="color: #fff"></i></button>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">




      <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
        <li class="nav-item nav-search d-none d-lg-block">
          <input type="text" class="form-control" id="navbar-search-input" placeholder="Search now" aria-label="search" aria-describedby="search">
        </li>

        <li class="nav-item" style="margin-left:5px">
          <div class="txt" style="color:white;"> <?= $this->session->userdata('username'); ?></div>
        </li>

        <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li> -->
      </ul>
    </div>
  </nav>
  <br>


  <div class="content-wrapper">
    <section class="content">
      <center>
        <h4><strong>DETAIL INVENTARIS</strong></h4>
      </center>
      <br>
      <?php foreach ($vAtaris as $i) { ?>
        <table class="table">
          <tr>
            <th>ID Inventaris</th>
            <td><?= $i['id_inventaris'] ?></td>
          </tr>
          <tr>
            <th>ID Petugas</th>
            <td><?= $i['nama_user'] ?></td>
          </tr>
          <tr>
            <th>ID Ruang</th>
            <td><?= $i['nama_ruang'] ?></td>
          </tr>
          <tr>
            <th>ID Jenis</th>
            <td><?= $i['nama_jenis'] ?></td>
          </tr>
          <tr>
            <th>Nama Barang</th>
            <td><?= $i['nama_barang'] ?></td>
          </tr>
          <tr>
            <th>Kondisi</th>
            <td><?= $i['kondisi'] ?></td>
          </tr>
          <tr>
            <th>Jumlah</th>
            <td><?= $i['jumlah'] ?></td>
          </tr>
          <tr>
            <th>Keterangan</th>
            <td><?= $i['keterangan'] ?></td>
          </tr>
          <tr>
            <th>Status</th>
            <td><?= $i['status'] ?></td>
          </tr>
          <tr>
            <th>Gambar</th>
            <td><img src="<?= base_url('assets/img/barangPinjam') . '/' . $i['foto'] ?>" width="100px;" alt=""></td>
          </tr>
        </table>

        <a href="<?= base_url('Admin/inven'); ?> "><input type="submit" class="btn btn-danger" style="margin-left:10px;margin-bottom: 20px;" value="Kembali"></a>
      <?php } ?>
    </section>
  </div><!-- /.row -->




</div>
</div>


</div>







<!-- /#page-content-wrapper -->

</div>

<!-- div4 -->





</div>