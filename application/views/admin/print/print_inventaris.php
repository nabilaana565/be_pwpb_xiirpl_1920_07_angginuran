<html>

<head>
    <title>Print Inventaris</title>
</head>

<body>

    <table class="table table-hover table-fixed">

        <!--Table head-->
        <thead class="thead-dark">
            <tr>
                <th>No</th>
                <th>Kode Inventaris</th>

                <th>Nama Barang</th>
                <th>Kondisi</th>
                <th>Keterangan</th>
                <th>Jumlah</th>
                <th>Status</th>





            </tr>
        </thead>
        <!--Table head-->

        <!--Table body-->
        <tbody>
            <?php $no = 1;
            foreach ($inventaris as $inven) : ?>

                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $inven['kode_inventaris'] ?></td>

                    <td><?= $inven['nama_barang'] ?>
                    </td>
                    <td><?= $inven['kondisi'] ?></td>
                    <td><?= $inven['keterangan'] ?></td>
                    <td><?= $inven['jumlah'] ?></td>
                    <td><?= $inven['status'] ?></td>

                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
    <script type="text/javascript">
        window.print();
    </script>
</body>

</html>