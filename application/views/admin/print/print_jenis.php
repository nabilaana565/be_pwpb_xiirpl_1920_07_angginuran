<html>

<head>
    <title>Print Jenis</title>
</head>

<body>
    <table class="table table-striped">

        <!--Table head-->
        <thead class="thead-dark">
            <tr>
                <th>No</th>
                <th>ID Ruang</th>
                <th>Nama Ruang</th>
                <th>Kode Ruang</th>
                <th>Keterangan</th>
                <th>Aksi</th>




            </tr>
        </thead>
        <!--Table head-->

        <!--Table body-->
        <tbody>
            <?php $no = 1;
            foreach ($jenis as $je) : ?>

                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $je['id_jenis'] ?></td>
                    <td><?= $je['nama_jenis'] ?></td>
                    <td><?= $je['kode_jenis'] ?></td>
                    <td><?= $je['keterangan'] ?></td>

                    <td>
                        <a href="<?= base_url('Admin/nampilEditjen/' . $je['id_jenis']) ?>">


                            <span class="fas fa-edit" aria-hidden="true" title="Edit" style="color:green;"></span>
                            </button>
                        </a>
                        <a href="<?= base_url('Admin/delljen/' . $je['id_jenis']) ?>" onclick="return confirm ('Hapus Data?'); ">

                            <span class="fa fa-trash" aria-hidden="true" title="Hapus" style="color:red;"></span>
                            </button>

                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>

    <script type="text/javascript">
        window.print();
    </script>
</body>

</html>