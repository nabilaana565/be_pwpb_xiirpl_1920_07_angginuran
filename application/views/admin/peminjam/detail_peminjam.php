<!DOCTYPE html>
<html>

<head>
    <title>Dashboard Admin</title>
    <link rel="stylesheet" href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/admin.css">
    <link rel="stylesheet" href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

    <nav class="navbar navbar-inverse" style="margin-bottom: 0px; background-color: #33AAC5; border-color: #33AAC5;">
        <div class="container-fluid" style="color: white;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="color: white;">Inventaris Sekolah</a>
            </div>

            <div class="collapse navbar-collapse" id="myNavbar" style="border-color: unset;">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" style="color: white;"><span class="glyphicon glyphicon-user"></span> <?= $this->session->userdata('username'); ?></a></li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="container-fluid">
        <div class="row content" style="height: 0;">
            <div class="col-sm-3" style="background-color: #BADDE8;">
                <br>
                <center>
                    <img src="../../assets/img/icon/admin.png" class="rounded-circle" alt="Cinque Terre">
                    <h2>Admin Dashboard</h2>
                </center>
                <br>
                <br>
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="admin.html">
                            <img src="../../assets/img/admin/home.png"> Dashboard Admin
                        </a></li>
                    <li><a href="inventaris.html">
                            <img src="../../assets/img/admin/inven.png">Inventaris</a></li>


                    <li><a href="peminjam.html">
                            <img src="../../assets/img/admin/user.png" style="width: 40px;">
                            Peminjam</a></li>
                    <li><a href="peminjaman.html">

                            <img src="../../assets/img/admin/jam.png" style="width: 40px;">Peminjaman</a></li>
                    <li><a href="pengembalian.html">

                            <img src="../../assets/img/admin/send.png" style="width: 40px;">Pengembalian</a></li>
                    <li><a href="ruang.html">
                            <img src="../../assets/img/admin/room.png">Ruang
                        </a></li>
                    <li><a href="jenis.html">
                            <img src="../../assets/img/admin/type.png"> Jenis
                        </a></li>
                    <li><a href="detail.html">

                            <img src="../../assets/img/admin/tail.png" style="width: 50px;"> Detail Barang</a></li>

                    <li><a href="../index.html">

                            <img src="../../assets/img/admin/exit.png" style="width: 50px;"> Log out</a></li>
                </ul><br>
            </div>

            <div class="col-sm-9">
                <br>
                <br>
                <br>
                <br>
                <br>


                <center>
                    <h1>Data Detail Inventaris</h1>
                </center>
                <br>
                <?php foreach ($peminjam as $p) { ?>
                    <div class="col-md-8">
                        <div class="well well-sm">Nama Barang :<?= $p['id_peminjam'] ?></div>
                        <div class="well well-sm">Kondisi :<?= $p['username'] ?></div>
                        <div class="well well-sm">jumlah : <?= $p['nama_peminjam'] ?></div>
                        <div class="well well-sm">Keterangan : <?= $p['password'] ?></div>
                        <div class="well well-sm">Status : <?= $p['id_level'] ?></div>

                        <a href="<?= base_url('Admin/peminjam'); ?> "><input type="submit" class="btn btn-danger" style="margin:0px 0%; margin-bottom: 20px;" value="cancel"></a>
                    </div>

                <?php } ?>
            </div>
        </div>

    </div>
    </div>
    </div>
    </div>
    </div>

    <footer class="container-fluid" style="background-color:  #33AAC5;">
        <center>
            <p>Footer Text</p>
        </center>
    </footer>

    <script src="../../assets/bootstrap/js/bootstrap.js">
    </script>
    <script src="../../assets/vendor/bootstrap/js/query.min.js"></script>

    <script src="../../assets/vendor/bootstrap/js/jquery.min.js"></script>

    <script src="../../assets/vendor/bootstrap/js/jsquery.min.js"></script>
</body>

</html>