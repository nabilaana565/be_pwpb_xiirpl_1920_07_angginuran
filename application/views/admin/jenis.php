<!DOCTYPE html>
<html>
<head>
    <title>Dashboard Admin</title>
 <link rel="stylesheet" href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../../assets/css/admin.css">
     <link rel="stylesheet" href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<nav class="navbar navbar-inverse" style="margin-bottom: 0px; background-color: #33AAC5; border-color: #33AAC5;">
  <div class="container-fluid" style="color: white;">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" style="color: white;">Inventaris Sekolah</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar"  style="border-color: unset;">
      <ul class="nav navbar-nav navbar-right">
    <li><a href="#" style="color: white;"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
      </ul>
    </div>
  </div>
</nav>
  

<div class="container-fluid">
  <div class="row content" style="height: 0;">
    <div class="col-sm-3" style="background-color: #BADDE8;">
        <br>
     <center>
        <img src="../../assets/img/icon/admin.png" class="rounded-circle" alt="Cinque Terre">
        <h2>Admin Dashboard</h2>
    </center> 
<br>
<br>
 <ul class="nav nav-pills nav-stacked">
       <li><a href="admin.html">
    <img src="../../assets/img/admin/home.png">  Dashboard Admin
    </a></li>
       <li><a href="inventaris.html">
            <img src="../../assets/img/admin/inven.png">Inventaris</a></li>


         <li><a href="peminjam.html">
        <img src="../../assets/img/admin/user.png"  style="width: 40px;">
    Peminjam</a></li>
    <li><a href="peminjaman.html">

        <img src="../../assets/img/admin/jam.png" style="width: 40px;">Peminjaman</a></li>
     <li><a href="pengembalian.html">

    <img src="../../assets/img/admin/send.png" style="width: 40px;">Pengembalian</a></li>
      <li><a href="ruang.html">
     <img src="../../assets/img/admin/room.png">Ruang
    </a></li>
        <li><a href="jenis.html">
    <img src="../../assets/img/admin/type.png"> Jenis
    </a></li>
         <li><a href="detail.html">

      <img src="../../assets/img/admin/tail.png" style="width: 50px;">  Detail Barang</a></li>

       <li><a href="../index.html">

      <img src="../../assets/img/admin/exit.png" style="width: 50px;">  Log out</a></li>
      </ul><br>
    </div>

    <div class="col-sm-9">
<br>
<br>


<center><h1>Jenis Inventaris</h1></center>
  <br>
  <br>
  <div class="row">
    <div class="col-md-6">
      <div class="col-md-2" style="margin-left: 30px;"> 
     <h5> Sort By </h5>
    </div>
      <div class="col-md-4">
<div class="form-group">
        <!--  <label for="sortBy">Sort By:</label> -->
    <select class="form-control" id="exampleFormControlSelect1" style="width: 100px;">
      <option>5</option>
      <option>10</option>
      <option>25</option>
      <option>50</option>
      <option>100</option>
    </select>
  </div>
       </div>
  </div>


    
    <div class="col-md-6">
    <nav class="navbar navbar-light">
      <form class="form-inline">
    <div class="input-group">
      <input type="search" class="form-control" size="30" placeholder="Search" required>
      <div class="input-group-btn">
        <button type="button" class="btn btn-info">Search</button>
      </div>
    </div>
  </form>
</nav>
  </div>
</div>
<div class="table-responsive">
<table class="table table-sm">
  <thead class="thead-dark">
   <tr>
      <th scope="col">No</th>
      <th scope="col">ID Invetaris</th>
      <th scope="col">Kode Inventaris</th>
      <th scope="col">ID Petugas</th>
      <th scope="col">ID Ruang</th>
      <th scope="col">ID Jenis</th>
    <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
       <td>Mark</td>
      <td>Otto</td>
      <td>
        <a href=""><button type="button" class="btn btn-success">Edit</button></a>
     <a href=""><button type="button" class="btn btn-warning">Tambah</button></a> 
    <a href=""><button type="button" class="btn btn-danger">Hapus</button></a></td>
       
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
      
      <td>Otto</td>
      <td>@mdo</td>
        <td>
        <a href=""><button type="button" class="btn btn-success">Edit</button></a>
     <a href=""><button type="button" class="btn btn-warning">Tambah</button></a> 
    <a href=""><button type="button" class="btn btn-danger">Hapus</button></a></td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry the Bird</td>
      <td>@twitter</td>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
        <td>
        <a href=""><button type="button" class="btn btn-success">Edit</button></a>
     <a href=""><button type="button" class="btn btn-warning">Tambah</button></a> 
    <a href=""><button type="button" class="btn btn-danger">Hapus</button></a></td>
    </tr>

    <tr>
      <th scope="row">4</th>
      <td>Otto</td>
      <td>@mdo</td>
       <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
        <td>
        <a href=""><button type="button" class="btn btn-success">Edit</button></a>
     <a href=""><button type="button" class="btn btn-warning">Tambah</button></a> 
    <a href=""><button type="button" class="btn btn-danger">Hapus</button></a></td>
    </tr>

    <tr>
      <th scope="row">5</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
       <td>Mark</td>
      <td>Otto</td>
       <td>
        <a href=""><button type="button" class="btn btn-success">Edit</button></a>
     <a href=""><button type="button" class="btn btn-warning">Tambah</button></a> 
    <a href=""><button type="button" class="btn btn-danger">Hapus</button></a></td>
    </tr>
  </tbody>
</table>
</div>
</div>
            </div>
          
        </div>
      </div>
    </div>
  </div>
</div>

<footer class="container-fluid" style="background-color:  #33AAC5;">
  <center><p>Footer Text</p></center>
</footer>

<script src="../../assets/bootstrap/js/bootstrap.js">
</script>
    <script src="../../assets/vendor/bootstrap/js/query.min.js"></script>

    <script src="../../assets/vendor/bootstrap/js/jquery.min.js"></script>

    <script src="../../assets/vendor/bootstrap/js/jsquery.min.js"></script>
</body>
</html>