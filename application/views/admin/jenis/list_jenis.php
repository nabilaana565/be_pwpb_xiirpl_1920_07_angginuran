<!-- Page Content -->
<div id="page-content-wrapper">

    <nav class="navbar navbar-expand-lg border-bottom" style="background-color: #33AAC5; ">
        <button class="btn" id="menu-toggle"><i class="fas fa-bars" style="color: #fff"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">




            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">


                <li class="nav-item" style="margin-left:5px">
                    <div class="txt" style="color:white;"> <?= $this->session->userdata('username'); ?></div>
                </li>

                <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li> -->
            </ul>
        </div>
    </nav>
    <br>
    <div class="row">
        <div class="col-md-6">

        </div>


        <div class="col-md-6">
            <nav class="navbar navbar-light">
                <form action="<?= base_url('Admin/search_jen') ?>" method="get">
                    <div class="input-group">
                        <input type="search" class="form-control" size="30" placeholder="Search" name="keywordjen" style="  background-color: none !important;
      background-image: none !important;
      color: rgb(0, 0, 0) !important;">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-info">Search</button>
                        </div>
                    </div>
                </form>
            </nav>
        </div>
    </div>
    <br>
    <center>
        <h2>Data Jenis Inventaris</h2>
    </center>
    <br>
    <div class="row left">
        <div class="table-responsive">
            <table class="table table-striped">

                <!--Table head-->
                <thead class="thead-dark">
                    <tr>
                        <th>No</th>
                        <th>ID Jenis</th>
                        <th>Nama Jenis</th>
                        <th>Kode Jenis</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>




                    </tr>
                </thead>
                <!--Table head-->

                <!--Table body-->
                <tbody>
                    <?php $no = 1;
                    foreach ($jenis as $je) : ?>

                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $je['id_jenis'] ?></td>
                            <td><?= $je['nama_jenis'] ?></td>
                            <td><?= $je['kode_jenis'] ?></td>
                            <td><?= $je['keterangan'] ?></td>

                            <td>
                                <a href="<?= base_url('Admin/nampilEditjen/' . $je['id_jenis']) ?>">


                                    <span class="fas fa-edit" aria-hidden="true" title="Edit" style="color:green;"></span>
                                    </button>
                                </a>
                                <a href="<?= base_url('Admin/delljen/' . $je['id_jenis']) ?>" onclick="return confirm ('Hapus Data?'); ">

                                    <span class="fa fa-trash" aria-hidden="true" title="Hapus" style="color:red;"></span>
                                    </button>

                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>

            </table>
        </div>

        <a class="btn btn-info" href="<?= base_url(); ?>Admin/nampilFormJen" style="margin-top:10px;">
            <span class="fas fa-plus" aria-hidden="true"></span> ADD JENIS
        </a>

        <a class="btn btn-danger" href="<?= base_url(); ?>Admin/print_jenis" style="margin-left:10px; margin-top:10px;">
            <span class="fa fa-print" aria-hidden="true"></span> Print Data Jenis
        </a>

    </div> <br> <br>
</div><!-- /.row -->



</div>
</div>


</div>







<!-- /#page-content-wrapper -->

</div>

<!-- div4 -->





</div>