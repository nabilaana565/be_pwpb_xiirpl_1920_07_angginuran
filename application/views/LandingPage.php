<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>Invens</title>
    <style>
        .panel-primary {
            border-color: white;
        }
    </style>
</head>

<body>
    <div id="nav">
        <nav class="navbar navbar-default navbar-fixed-top" style="background-color:  #adcae5; ">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">
                    <div style="color:white;">INVENS</div>
                </a>
                <!-- <a class="navbar-brand" href="#">
                    <i class="fa fa-pinterest-square" style="font-size:36px;color:red"></i>
                </a>
                <a class="navbar-brand" href="#">
                    <i class="fa fa-twitter" style="font-size:36px;color:blue"></i>
                </a>
                <a class="navbar-brand" href="#">
                    <i class="fa fa-git" style="font-size:36px;color:black;"></i>
                </a> -->
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right" style="margin-right:10px;">
                    <!-- <a href="<?= base_url(); ?>Auth/register"> <button type="button" class="btn btn-default navbar-btn">Register</button> </a> -->
                    <a href="<?= base_url(); ?>Auth/index"> <i class="glyphicon glyphicon-user" title="Login" style="font-size:25px; margin-top:10px; color:whitesmoke;"></i></a>
                </ul>

            </div>
        </nav>
        <br>
        <br>

        <!-- div col tulisan -->
        <div class="col-md-6">
            <div class="jumbotron jumbotron-fluid" style="background-color: #fff; margin-top: 100px;">
                <div class="container">
                    <h1 class="display-4" style="font-family: Cambria Math;">Inventaris Sekolah</h1>
                    <p class="lead">Inventarisasi sarana dan prasarana pendidikan adalah pencatatan atau pendaftaran barang-barang milik sekolah ke dalam suatu daftar inventaris barang secara tertib dan teratur menurut ketentuan dan tata cara yang berlaku.</p>
                </div>
            </div>
        </div>
        <!-- tutupan col -->

        <!-- col gambar -->
        <br>
        <div class="col-md-6">
            <div class="thumbnail1"></div>
        </div>
        <!-- tutupan col gambar -->
    </div>

    <!-- atas finish -->


    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <center><img src="assets/img/icon/Rectangle 2.3.jpg" class="img-responsive" style="width:50%" alt="Image"></center>
                        <p class="text-center"><strong>User Inventaris</strong></p><br>

                    </div>
                    <!-- <div class="panel-footer">inventarisasi barang adalah semua kegiatan dan usaha untuk memperoleh data yang diperlukan tentang ketersediaan barang-barang yang dimiliki dan diurus yang telah ditetapkan di masing-masing instansi.</div> -->
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-primary">
                    <!-- <div class="panel-heading">Arsip Data</div> -->
                    <div class="panel-body">
                        <center><img src="assets/img/icon/arsip.png" class="img-responsive" style="width:50%" alt="Image"></center>
                        <p class="text-center"><strong>Data Inventaris</strong></p><br>

                    </div>
                    <!-- <div class="panel-footer">Arsip data berupa disket atau media penyimpan digital lainnya yang berisikan data transaksi, data buku besar, dan/atau data lainnya yang terekam pada kertas (kartu, formulir, surat-surat), kertas film, media komputer (disket, harddisk, piringan).</div> -->
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-primary">
                    <!-- <div class="panel-heading">Peminjaman</div> -->
                    <div class="panel-body">
                        <center><img src="assets/img/icon/Rectangle 2.3.png" class="img-responsive" style="width:50%" alt="Image"></center>
                        <br>
                        <p class="text-center"><strong>Barang Inventaris</strong></p><br>

                    </div>
                    <!-- <div class="panel-footer">Daftar peminjaman barang inventaris sekolah merupakan suatu lembaran yang dapat digunakan untuk mencatat/mendata setiap barang yang keluar (dipinjam) dan yang masuk (dikembalikan) oleh warga di lingkungan internal sekolah untuk keperluannya.</div> -->
                </div>
            </div>
        </div>
        <br>

        <div class="jumbotron" style="background-color: #adcae5;">
            <center>
                <h1>INVENTARIS SEKOLAH</h1>
                <p>SMKN NEGERI 1 KOTA BEKASI</p>
                <a class="btn btn-primary btn-lg" href="#" role="button">Selengkapnya</a>

            </center>
        </div>


    </div>


    </div>

    </div>

    <div class="footer">
        <h3 class="teks"> Copyright</h3>
        <!-- <div class="col-md-6" style="background-color: blue; margin-top: 50px; height: 80px;">
  <h4 style="margin: auto;"> INVENTARIS SEKOLAH </h4> 
  </div>
  <div class="col-md-6" style="background-color: red; margin-top: 50px; height: 80px;">
  <i class="fa fa-twitter" style="font-size:36px;color:blue"></i>
  <i class="fa fa-twitter" style="font-size:36px;color:blue"></i>
  <i class="fa fa-twitter" style="font-size:36px;color:blue"></i>
  </div>
  <div class="col-md-6" style="background-color: yellow;"></div> -->
    </div>
    <script src="assets/vendor/bootstrap/js/query.min.js"></script>

    <script src="assets/vendor/bootstrap/js/jquery.min.js"></script>

    <script src="assets/vendor/bootstrap/js/jsquery.min.js"></script>
</body>

</html>