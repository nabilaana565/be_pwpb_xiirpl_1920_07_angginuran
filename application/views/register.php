<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/css/style.css">

  <title>Register</title>
</head>

<body id="bc">

  <div class="fo">
    <br>
    <center>
      <h2>Register</h2>
    </center>
    <br>
    <form style="margin-top: 10px;" action="<?= base_url('Auth/register'); ?>" method="post">

      <div class="form-group">

        <input type="text" class="form-control" id="namaPengguna" placeholder="Nama Pengguna" name="namaPengguna" autofocus="autofocus" value="<?= set_value('namaPengguna'); ?>">
        <?= form_error('namaPengguna'); ?>
      </div>

      <div class="form-group">

        <input type="text" class="form-control" id="username" placeholder="username" name="username" autofocus="autofocus" value="<?= set_value('username'); ?>">
      </div>
      <div class="form-group">

        <input type="password" class="form-control" id="password" placeholder="Password" name="password" autofocus="autofocus">
      </div>
      <button type="submit" value="login" class="btn btn-primary btn-block" style="width:70%; margin:auto;">Masuk</button>

      <br>
      <br>
    </form>
  </div>

  <script src="../bootstrap/js/bootstrap.js"></script>
  <script src="../bootstrap/js/npm.js"></script>

  <script src="../bootstrap/js/bootstrap.min.js"></script>
</body>

</html>