<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/css/style.css">

  <title>Log in</title>
</head>

<body id="bc">

  <div class="fo">
    <img src="../assets/img/icon/download.png" class="iglogin" style="margin-top: 10px;">
    <h2>
      <center>LOG IN </center>
    </h2>
    <br>
    <?php echo form_open('Auth/proses_login'); ?>
    <?= $this->session->flashdata('message'); ?>
    <form style="margin-top: 10px;" action="<?= base_url(); ?>login/index">
      <div class="form-group">
        <input type="text" class="form-control" id="username" placeholder="Masukan Username " name="username" required="required" autofocus="autofocus" value="<?= set_value('username') ?>">
        <?= form_error('username', '<small class="text-danger pl-3">', '</small>') ?>
      </div>
      <div class="form-group">

        <input type="password" class="form-control" id="pwd" placeholder="Password" name="password" required="required" autofocus="autofocus">
      </div>
      <button type="submit" class="btn btn-primary btn-block" style="width:70%; margin:auto;">Masuk</button>

      <br>
      <br>
    </form>
    <?php form_close() ?>
  </div>
  <script src="bootstrap/js/bootstrap.js"></script>
  <script src="bootstrap/js/npm.js"></script>

  <script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>