<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/admin.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="../assets/css/style.css"> -->
    <title>Peminjam</title>

    <style>
        .img-thumbnail {
            border-style: unset;
        }
    </style>

    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

</head>

<body>

    <nav class="navbar navbar-inverse" style="margin-bottom: 0px; background-color: #33AAC5; border-color: #33AAC5;">
        <div class="container-fluid" style="color: white;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="color: white;">Inventaris Sekolah</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar" style="border-color: unset;">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: white;"><span class="glyphicon glyphicon-user"></span> <?= $this->session->userdata('username'); ?></a></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url(); ?>Home">Log out</a></li>
                        </ul>
            </div>
        </div>
    </nav>

    <!-- <div class="float-sm-right">
<button type="button" class="btn btn-none" data-toggle="modal" data-target=".bd-example-modal-sm">Dean R Hidayat</button>

<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
     Edit akun 
    </div>
  </div>
</div>
<a href="../../index.html">
  <img src="../../assets/img/icon/exit.jpg" class="out">
</a>
</div> -->


    <br>
    <div class="container">
        <!-- Container (Portfolio Section) -->

        <div class="row text-center">
            <div class="content-wrapper">
                <section class="content">
                    <center>
                        <h4><strong>DETAIL INVENTARIS</strong></h4>
                    </center>
                    <br>
                    <?php foreach ($vtaris as $i) { ?>
                        <table class="table">
                            <tr>
                                <th>Kode Barang</th>
                                <td><?= $i['kode_inventaris'] ?></td>
                            </tr>
                            <tr>
                                <th>Nama Barang</th>
                                <td><?= $i['nama_barang'] ?></td>
                            </tr>
                            <tr>
                                <th>Kondisi</th>
                                <td><?= $i['kondisi'] ?></td>
                            </tr>
                            <tr>
                                <th>Jumlah</th>
                                <td><?= $i['jumlah'] ?></td>
                            </tr>
                            <tr>
                                <th>Keterangan</th>
                                <td><?= $i['keterangan'] ?></td>
                            </tr>
                            <tr>
                                <th>Gambar</th>
                                <td><img src="<?= base_url('assets/img/barangPinjam') . '/' . $i['foto'] ?>" width="100px;" alt=""></td>
                            </tr>
                        </table>
                        <br>
                        <a href="<?= base_url('Peminjam/nampilFormPem/' . $i['id_inventaris']); ?>"><input type="submit" class="btn btn-info" style="margin-left:10px;margin-bottom: 20px;" value="Pinjam"></a>
                        <a href="<?= base_url('peminjam/index'); ?>"><input type="submit" class="btn btn-danger" style="margin-left:10px;margin-bottom: 20px;" value="Kembali"></a>

                    <?php } ?>
                    <!-- <center>
                        <?php if ($i->jumlah == 0) : ?>
                            <input type="button" class="btn btn-danger" style="margin:0px 0%; margin-bottom: 20px;" value="Barang Habis">

                        <?php else : ?>
                            <a href="<?= base_url(); ?>peminjam/nampilFormPem"> <input type="button" class="btn btn-danger" style="margin:0px 0%; margin-bottom: 20px;" value="pinjam"></a>
                            <a href="<?= base_url(); ?>peminjam/index"> <input type="button" class="btn btn-danger" style="margin:0px 0%; margin-bottom: 20px;" value="cancel"></a>
                        <?php endif ?>
                    </center> -->
                </section>

            </div><!-- /.row -->
        </div>
    </div>


    <br>
    <br> <br> <br>

    <footer class="container-fluid" style="background-color: #33AAC5;">
        <center>
            <p>Footer Text</p>
        </center>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= base_url(); ?>assets/vendor/bootstrap/js/query.min.js"></script>

    <script src="<?= base_url(); ?>assets/vendor/bootstrap/js/jquery.min.js"></script>

    <script src="<?= base_url(); ?>assets/vendor/bootstrap/js/jsquery.min.js"></script>



</body>

</html>