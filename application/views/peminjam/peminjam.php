<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <!-- <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/admin.css">
  <link rel="stylesheet" href="<?= base_url(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css">
  <!-- <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/style.css"> -->
  <title>Peminjam</title>

  <style>
    .img-thumbnail {
      border-style: unset;
    }
  </style>

  <script>
    $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip();
    });
  </script>

</head>

<body>

  <nav class="navbar navbar-inverse" style="margin-bottom: 0px; background-color: #33AAC5; border-color: #33AAC5;">
    <div class="container-fluid" style="color: white;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" style="color: white;">Inventaris Sekolah</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar" style="border-color: unset;">
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: white;"><span class="glyphicon glyphicon-user"> </span> <?= $this->session->userdata('nama_peminjam'); ?></a>
            <ul class="dropdown-menu">
              <li><a href="<?= base_url('Auth/logout'); ?>">Log out</a></li>
            </ul>
      </div>
    </div>
  </nav>

  <!-- <div class="float-sm-right">
<button type="button" class="btn btn-none" data-toggle="modal" data-target=".bd-example-modal-sm">Dean R Hidayat</button>

<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
     Edit akun 
    </div>
  </div>
</div>
<a href="<?= base_url(); ?>/<?= base_url(); ?>/index.html">
  <img src="<?= base_url(); ?>/<?= base_url(); ?>/assets/img/icon/exit.jpg" class="out">
</a>
</div> -->


  <br>
  <div class="container">
    <h1> Data Inventaris</h1>

    <div class="row">
      <div class="col-md-6">

      </div>

      <div class="col-md-6">
        <nav class="navbar navbar-light">
          <form action="<?= base_url('Peminjam/search_inventaris') ?>" method="get">
            <div class="input-group">
              <input type="search" class="form-control" size="30" placeholder="Search Berdasarkan Nama Barang" name="keywordinventaris" style="  background-color: none !important;
      background-image: none !important;
      color: rgb(0, 0, 0) !important;">
              <div class="input-group-btn">
                <button type="submit" class="btn btn-info">Search</button>
              </div>
            </div>
          </form>
        </nav>
      </div>
    </div>

    <!-- Container (Portfolio Section) -->


    <div class="row text-center">
      <?php foreach ($vtaris as $inven) : ?>
        <div class="col-sm-4">
          <div class="panel panel-info">
            <div class="panel-heading">BARANG INVENTARIS</div>

            <img src="<?= base_url(); ?>assets/img/barangPinjam/<?= $inven['foto'] ?>" class="img-thumbnail" data-toggle="tooltip" alt="Paris" alt="<?= base_url(); ?>.">
            <p><?= $inven['nama_barang'] ?></p>
            <p><?= $inven['nama_jenis'] ?></p>
            <div class="panel-footer"><a href="<?= base_url('Peminjam/nampilFormPem/' . $inven['id_inventaris']); ?>"><input type="submit" class="btn btn-basic" style="margin-left:10px;margin-bottom: 20px;" value="Pinjam"></a>
              <a href="<?= base_url('Peminjam/aksidetail_pnjmn/' . $inven['id_inventaris']) ?>"><input type="submit" class="btn btn-basic" style="margin-left:10px;margin-bottom: 20px;" value="Detail"></a></div>
          </div>
        </div>
      <?php endforeach; ?>

    </div>
  </div>


  <br>

  <footer class="container-fluid" style="background-color: #33AAC5;">
    <center>
      <p>Footer Text</p>
    </center>
  </footer>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="<?= base_url(); ?>/assets/vendor/bootstrap/js/query.min.js"></script>

  <script src="<?= base_url(); ?>/assets/vendor/bootstrap/js/jquery.min.js"></script>

  <script src="<?= base_url(); ?>/assets/vendor/bootstrap/js/jsquery.min.js"></script>



</body>

</html>