<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">

  <title>Add Inventaris</title>
  <style>
    .form-control {
      width: 80%;
    }

    label {
      margin-left: 20px;
    }

    .input-group {
      margin-left: 10px;
      margin-right: 10px;


    }
  </style>
</head>

<body id="rg">

  <!-- <a href="peminjaman.html">
        <img src="../assets/img/icon/panah.png" title="kembali" class="keluar" style="margin: 20px;">
    </a> -->

  <div class="fp">
    <div class="t1" style="background-color:#BADDE8; height: 30%; padding: 8px 0px;">
      <h2>
        <center style="color:white;">PINJAM BARANG</center>
      </h2>

    </div>
    <br>

    <?php foreach ($inventaris as $i) { ?>
      <form style="margin-top: 10px;" action="<?= base_url('Peminjam/tambah_pjm'); ?> " method="post" enctype="multipart/form-data">
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-calendar"></i></span>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon2" name="id_peminjam" value="<?= $this->session->userdata('username'); ?>">
        </div> <br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-calendar"></i></span>
          <input type="hidden" class="form-control" placeholder="Username" aria-describedby="sizing-addon2" name="id_inventaris" value="<?= $i['id_inventaris'] ?>">
          <input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon2" name="nama_inventaris" value="<?= $i['nama_barang'] ?>">
        </div> <br>



        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-calendar"></i></span>
          <input type="date" class="form-control" placeholder="Username" aria-describedby="sizing-addon2" name="tanggal_kembali" title="Tanggal Kembali" min="<?= date('Y-m-d') ?>" max="<?= date('Y-m-d', time() + (60 * 60 * 24 * 2)) ?>"></div> <br>

        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-calendar"></i></span>
          <input type="text" class="form-control" placeholder="Jumlah Barang" aria-describedby="sizing-addon2" name="jumlah">
        </div> <br>


        <input type="submit" class="btn btn-primary" style="margin:0px 3%; margin-bottom: 20px;" value="kirim" onclick="return confirm ('Simpan Perubahan?'); ">
        <a href="<?= base_url(); ?>peminjam/index"> <input type="button" class="btn btn-danger" style="margin:0px 0%; margin-bottom: 20px;" value="cancel" onclick="return confirm ('Batalkan Perubahan?'); "></a>
      </form>
    <?php } ?>
  </div> <br>
  <br>
  <script src="<?= base_url(); ?>bootstrap/js/bootstrap.js"></script>
  <script src="<?= base_url(); ?>bootstrap/js/npm.js"></script>

  <script src="<?= base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
</body>

</html>