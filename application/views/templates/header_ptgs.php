<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Inventaris Sekolah</title>


    <!-- Bootstrap core CSS -->
    <link href="<?= base_url(); ?>assets/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?= base_url(); ?>assets/admin/css/sidebar.css" rel="stylesheet">
    <!-- <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet"> -->
    <link href="<?= base_url(); ?>assets/admin/css/landingpage.min.css" rel="stylesheet">

    <style>
        .kotak1 {
            background-color: #9F71FF;
            width: 100%;
            height: 100px;
            border-radius: 3px;
        }

        .kotak2 {
            background-color: #0BF275;
            width: 100%;
            height: 100px;
            border-radius: 3px;

        }

        .kotak3 {
            background-color: #DCD300;
            width: 100%;
            height: 100px;
            border-radius: 3px;

        }
    </style>

</head>

<body>

    <div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div class="border-right" id="sidebar-wrapper">
            <div class="sidebar-heading" style="color:#fff;background-color: #33AAC5; ">INVENS</div>

            <center>
                <div class="sidebar-heading" style="color:#fff;background-color: rgb(255, 255, 255);margin:10px;margin-bottom: 0;width: 160px">
                    <img src="<?= base_url('assets/img/fotoProfil/'); ?><?= $this->session->userdata('ava'); ?>" style="background-size:cover;width: 100%;border-radius: 150px">
                </div>

                <div class="sidebar-heading" style="color:#000;background-color: rgb(255, 255, 255);width: 200px;text-align: center;font-family: calibri;margin-left: 10px;margin-right: 10px">
                    <h5><?= $this->session->userdata('nama_user'); ?></h5>
                    <p style="color:rgb(180, 177, 177);font-size: 12pt">Petugas</p>

                </div>
            </center>

            <div class="list-group list-group-flush">
                <a href="<?= base_url(); ?>petugas/index" class="list-group-item list-group-item-actions a"><i class="fa fa-home"></i>
                    Dashboard</a>
                <a href="<?= base_url(); ?>petugas/inventaris" class="list-group-item list-group-item-action a"><i class="fa fa-ellipsis-h"></i> Inventaris</a>
                <a href="<?= base_url(); ?>petugas/peminjaman" class="list-group-item list-group-item-action a"><i class="fa fa-ellipsis-h"></i> Peminjaman</a>
                <a href="<?= base_url(); ?>petugas/pengembalian" class="list-group-item list-group-item-action a"><i class="fa fa-ellipsis-h"></i> Pengembalian</a>
                <a href="<?= base_url(); ?>petugas/peminjam" class="list-group-item list-group-item-action a"><i class="fa fa-ellipsis-h"></i> Peminjam</a>

                <a href="<?= base_url('Auth/logout'); ?><?= $this->session->flashdata('message'); ?>" class="list-group-item list-group-item-action a"><i class="fas fa-sign-out-alt"></i> Logout</a>
            </div>
        </div>
        <!-- /#sidebar-wrapper -->