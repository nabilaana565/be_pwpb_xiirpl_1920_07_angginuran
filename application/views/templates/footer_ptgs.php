<div class="container-fluid" style="background-color: #33AAC5;height: 50px;">
    <center>
        <p class="white fontfot" style="margin-top:0;padding-top:10px">@copyRight</p>
    </center>
</div>

<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="<?= base_url(); ?>assets/admin/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $('#dtBasicExample').mdbEditor({
        mdbEditor: true
    });
    $('.dataTables_length').addClass('bs-select');
</script>


</body>

</html>