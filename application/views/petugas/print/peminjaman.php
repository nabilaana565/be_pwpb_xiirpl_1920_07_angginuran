<html>

<head>
    <title>Print Peminjaman </title>
</head>

<body>
    <center>
        <h1>DATA PEMINJAMAN</h1>
    </center>
    <table class="table table-striped">

        <!--Table head-->
        <thead class="thead-dark">
            <tr>
                <th>No</th>

                <th>Nama Peminjam</th>
                <th>Tanggal kembali</th>
                <th>Status</th>




            </tr>
        </thead>
        <!--Table head-->

        <!--Table body-->
        <tbody>
            <?php $no = 1;
            foreach ($vPe as $pem) : ?>

                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $pem['nama_peminjam'] ?></td>
                    <td><?= $pem['tanggal_kembali'] ?></td>
                    <td><?= $pem['status_peminjaman'] ?></td>

                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
    <script type="text/javascript">
        window.print();
    </script>
</body>

</html>