<html>

<head>
    <title>Print pengembalian </title>
</head>

<body>
    <center>
        <h1>DATA PENGEMBALIAN</h1>
    </center>
    <table class="table table-sm">
        <thead class="thead-dark">
            <tr>
                <th>No</th>
                <th>Nama Peminjam</th>
                <th>Nama Barang </th>
                <th>Tanggal Kembali</th>
                <th>Jumlah</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1;
            foreach ($vPeng as $peng) : ?>

                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $peng['nama_peminjam'] ?></td>
                    <td><?= $peng['nama_barang'] ?></td>
                    <td><?= $peng['tanggal_kembali'] ?></td>
                    <td><?= $peng['jumlah'] ?></td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>
    <script type="text/javascript">
        window.print();
    </script>
</body>

</html>