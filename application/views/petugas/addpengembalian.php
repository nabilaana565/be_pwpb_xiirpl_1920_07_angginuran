<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/css/style.css">

  <title>Add Pengembalian</title>
  <style>
    .form-control {
      width: 80%;
    }

    label {
      margin-left: 20px;
    }

    .input-group {
      margin-left: 10px;
      margin-right: 10px;


    }
  </style>
</head>

<body id="rg">

  <!-- <a href="peminjaman.html">
        <img src="../assets/img/icon/panah.png" title="kembali" class="keluar" style="margin: 20px;">
    </a> -->

  <div class="fp">
    <div class="t1" style="background-color:#BADDE8; height: 30%; padding: 8px 0px;">
      <h2>
        <center style="color:white;">ADD PENGEMBALIAN</center>
      </h2>

    </div>
    <br>
    <form style="margin-top: 10px;" action="<?= base_url('petugas/aksiAddPeng'); ?> " method="post" enctype="multipart/form-data">


      <br>
      <div class="form-group row">
        <select class="form-control" id="sel1" name="id_peminjam">
          <option selected disabled>Nama Peminjam</option>
          <?php foreach ($peminjam as $p) : ?>
            <option value="<?= $p['id_peminjam'] ?>"><?= $p['nama_peminjam']; ?></option>
          <?php endforeach; ?>
        </select>
        <br>
        <select class="form-control" id="sel1" name="id_peminjaman">
          <option selected disabled>Status Peminjaman</option>
          <?php foreach ($peminjaman as $pem) : ?>
            <option value="<?= $pem['id_peminjaman'] ?>"><?= $pem['status_peminjaman']; ?></option>
          <?php endforeach; ?>
        </select>
        <br>
        <select class="form-control" id="sel1" name="id_inventaris">
          <option selected disabled>Nama Barang</option>
          <?php foreach ($inventaris as $i) : ?>
            <option value="<?= $i['id_inventaris'] ?>"><?= $i['nama_barang']; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="input-group">
        <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-align-justify"></i></span>
        <input type="text" class="form-control" placeholder="jumlah" aria-describedby="sizing-addon2" name="jumlah">
      </div>
      <br>
      <input type="submit" class="btn btn-primary" style="margin:0px 3%; margin-bottom: 20px;" value="kirim" onclick="return confirm ('Simpan Perubahan?'); ">
      <a href="<?= base_url(); ?>petugas/peminjaman"> <input type="button" class="btn btn-danger" style="margin:0px 0%; margin-bottom: 20px;" value="cancel" onclick="return confirm ('Batalkan Perubahan?'); "></a>

    </form>
  </div> <br>
  <br>
  <script src="<?= base_url(); ?>/bootstrap/js/bootstrap.js"></script>
  <script src="<?= base_url(); ?>/bootstrap/js/npm.js"></script>

  <script src="<?= base_url(); ?>/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>