<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/admin.css">

    <title>Data Peminjaman</title>
</head>

<body style="background-color: #BADDE8;">

    <nav class="navbar navbar-inverse" style="background-color: #33AAC5; border-color: #33AAC5;"">
  <div class=" container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand" style="color: white;">Inventaris Sekolah</div>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="<?= base_url(); ?>petugas/inventaris" style="color: white; font-size: 13pt;">Inventaris</a></li>
                <li><a href="<?= base_url(); ?>petugas/peminjaman" style="color: white; font-size: 13pt;">Peminjaman</a></li>
                <li><a href="<?= base_url(); ?>petugas/pengembalian" style="color: white; font-size: 13pt;">Pengembalian</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: white;"><span class="glyphicon glyphicon-user"></span> <?= $this->session->userdata('username'); ?></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Log out</a></li>
                    </ul>
        </div>
        </div>
    </nav>
    <br>
    <br>
    <br>
    <div class="container" style="background-color: white;">
        <br><br>



        <div class="container">
            <div class="table-responsive">

                <center>
                    <h4><strong>DETAIL PENGEMBALIAN</strong></h4>
                </center>
                <br>
                <?php foreach ($vPeng as $peng) { ?>
                    <table class="table">
                        <tr>
                            <th>ID Peminjaman</th>
                            <td><?= $peng['id_pengembalian'] ?></td>
                        </tr>
                        <tr>
                            <th>ID Peminjam</th>
                            <td><?= $peng['nama_peminjam'] ?></td>
                        </tr>
                        <tr>
                            <th>Nama Barang</th>
                            <td><?= $peng['nama_barang'] ?></td>
                        </tr>

                        <tr>
                            <th>Jumlah</th>
                            <td><?= $peng['jumlah'] ?></td>
                        </tr>


                        <tr>
                            <th>Tanggal kembali</th>
                            <td><?= $peng['tanggal_kembali'] ?></td>
                        </tr>
                        <tr>
                            <th>Status Peminjaman</th>
                            <td><?= $peng['status_peminjaman'] ?></td>
                        </tr>

                    </table>

                    <a href="<?= base_url('petugas/pengembalian'); ?> "><input type="submit" class="btn btn-danger" style="margin-left:10px;margin-bottom: 20px;" value="Kembali"></a>
                <?php } ?>
            </div>

        </div>
        <br>
        <br>
    </div>



    <script src="<?= base_url(); ?>/assets/vendor/bootstrap/js/query.min.js"></script>

    <script src="<?= base_url(); ?>/assets/vendor/bootstrap/js/jquery.min.js"></script>

    <script src="<?= base_url(); ?>/assets/vendor/bootstrap/js/jsquery.min.js"></script>
</body>

</html>