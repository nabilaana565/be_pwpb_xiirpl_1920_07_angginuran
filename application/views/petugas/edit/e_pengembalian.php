<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/style.css">

    <title>Edit pengembalian</title>
    <style>
        .form-control {
            width: 80%;
        }

        label {
            margin-left: 20px;
        }

        .input-group {
            margin-left: 10px;
            margin-right: 10px;


        }
    </style>
</head>

<body id="rg">

    <!-- <a href="pengembalian.html">
        <img src="<?= base_url(); ?>/assets/img/icon/panah.png" title="kembali" class="keluar" style="margin: 20px;">
    </a> -->

    <div class="fp">
        <div class="t1" style="background-color:#BADDE8; height: 30%; padding: 8px 0px;">
            <h2>
                <center style="color:white;">Edit pengembalian</center>
            </h2>

        </div>
        <br>



        <?php foreach ($pengembalian as $peng) { ?>
            <form style="margin-top: 10px;" action="<?= base_url(); ?>petugas/aksiEditPeng " method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <div class="col-md-4">
                        <label for="sel1">Nama Peminjam</label>
                        <select class="form-control" id="sel1" name="id_peminjam">
                            <?php foreach ($peminjam as $p) : ?>
                                <option value="<?= $p['id_peminjam'] ?>"><?= $p['nama_peminjam']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="sel1">Status Peminjaman</label>
                        <select class="form-control" id="sel1" name="id_peminjaman">
                            <?php foreach ($peminjaman as $pem) : ?>
                                <option value="<?= $pem['id_peminjaman'] ?>"><?= $pem['status_peminjaman']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="sel1">Nama Barang</label>
                        <select class="form-control" id="sel1" name="id_inventaris">
                            <?php foreach ($inventaris as $i) : ?>
                                <option value="<?= $i['id_inventaris'] ?>"><?= $i['nama_barang']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="input-group">
                    <input type="hidden" class="form-control" placeholder="id_pengembalian" aria-describedby="sizing-addon2" name="id_pengembalian" value="<?= $peng->id_pengembalian ?>">
                </div>
                <br>


                <div class="input-group">
                    <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-align-justify"></i></span>
                    <input type="text" class="form-control" placeholder="jumlah" aria-describedby="sizing-addon2" name="jumlah" value="<?= $peng->jumlah ?>">
                </div>
                <br>

                <input type="submit" class="btn btn-primary" style="margin:0px 3%; margin-bottom: 20px;" value="kirim" onclick="return confirm ('Simpan Perubahan?'); ">
                <a href="<?= base_url(); ?>petugas/pengembalian"> <input type="button" class="btn btn-danger" style="margin:0px 0%; margin-bottom: 20px;" value="cancel" onclick="return confirm ('Batalkan Perubahan?'); "></a>

            </form>
        <?php  } ?>
    </div> <br>
    <br>
    <script src="<?= base_url(); ?>bootstrap/js/bootstrap.js"></script>
    <script src="<?= base_url(); ?>bootstrap/js/npm.js"></script>

    <script src="<?= base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
</body>

</html>