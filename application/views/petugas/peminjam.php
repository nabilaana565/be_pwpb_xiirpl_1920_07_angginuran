<!-- Page Content -->
<div id="page-content-wrapper">

    <nav class="navbar navbar-expand-lg border-bottom" style="background-color: #33AAC5; ">
        <button class="btn" id="menu-toggle"><i class="fas fa-bars" style="color: #fff"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">




            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">


                <li class="nav-item" style="margin-left:5px">
                    <a class="nav-link" href="#" style="color:white;"> <?= $this->session->userdata('username'); ?><span class="sr-only"></span></a>
                </li>

                <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li> -->
            </ul>
        </div>
    </nav>
    <br>
    <div class="row">
        <div class="col-md-6">
        </div>


        <div class="col-md-6">
            <nav class="navbar navbar-light">
                <form class="form-inline">
                    <div class="input-group">
                        <input type="search" class="form-control" size="30" placeholder="Search" required>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info">Search</button>
                        </div>
                    </div>
                </form>
            </nav>
        </div>
    </div>
    <br>
    <h2>
        <center>Data Peminjam</center>
    </h2>
    <br>
    <div class="row left">


        <br>
        <br>
        <div class="table-responsive">
            <table class="table table-striped">

                <!--Table head-->
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Username</th>
                        <th scope="col">Nama Peminjam</th>


                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($peminjam as $pe) : ?>

                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $pe['username'] ?></td>
                            <td><?= $pe['nama_peminjam'] ?></td>

                            <!-- <td>
                                <a href="<?= base_url('Admin/aksidetail_peminjam/' . $pe['id_peminjam']) ?>">
                                    <span class="fa fa-info-circle" aria-hidden="true" title="Detail"></span>
                                    </button>

                                </a>
                                <a href="<?= base_url('Admin//' . $pe['id_peminjam']) ?>" onclick="return confirm ('Hapus Data?'); ">

                                    <span class="fa fa-trash" aria-hidden="true" title="Hapus" style="color:red;"></span>
                                    </button>

                                </a>
                            </td> -->


                        </tr>
                    <?php endforeach; ?>
                </tbody>

            </table>
        </div>
        <a class="btn btn-info" href="<?= base_url(); ?>petugas/nampilFormPem">

            <span class="fas fa-plus" aria-hidden="true"></span> ADD Peminjam

        </a>
    </div> <br> <br>
</div><!-- /.row -->




</div>
</div>


</div>







<!-- /#page-content-wrapper -->

</div>

<!-- div4 -->





</div>